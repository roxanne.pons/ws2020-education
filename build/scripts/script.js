// data.js a inistialisé une var 'questionnaire'

function rechercheQuestionParId(id){
  return questionnaire.questions.find( questionAct => id == questionAct.id);
}
function afficheResultatQuestionnaire(){
  var urlVideos = [];
  switch(texteCheminement) {
    //SPORT
    case '<li>Start</li><li>Faire</li><li>Sport</li><li>Débutant</li><li>Avec équipement</li><li>Musculation</li><li>Bas du corps</li>':
        urlVideos.push('https://www.youtube.com/embed/ZtvFNaQcwkc');
        urlVideos.push('https://www.youtube.com/embed/LuZ9e3EBYOg');
        urlVideos.push('https://www.youtube.com/embed/iqT_G1ci2QU');
        break;

    case '<li>Start</li><li>Faire</li><li>Sport</li><li>Débutant</li><li>Avec équipement</li><li>Musculation</li><li>Haut du corps</li>':
        urlVideos.push('https://www.youtube.com/embed/rgxbWGdobOc');
        urlVideos.push('https://www.youtube.com/embed/va6lh5nZ4R8');
        urlVideos.push('https://www.youtube.com/embed/ixKnTL8RFSI');
        break;
    
        case '<li>Start</li><li>Faire</li><li>Sport</li><li>Débutant</li><li>Avec équipement</li><li>Perte de graisse</li>':
        urlVideos.push('https://www.youtube.com/embed/LwKqBqN8Wyc');
        urlVideos.push('https://www.youtube.com/embed/tf1tdCYp6X8');
        urlVideos.push('https://www.youtube.com/embed/wJWxLp-g4rM');
        break;

    case '<li>Start</li><li>Faire</li><li>Sport</li><li>Débutant</li><li>Sans équipement</li><li>Perte de graisse</li>':
        urlVideos.push('https://www.youtube.com/embed/jE57-u2wRC8');
        urlVideos.push('https://www.youtube.com/embed/6BUTI9yCz-8');
        urlVideos.push('https://www.youtube.com/embed/HjWxrCOyMAs');
        urlVideos.push('https://www.youtube.com/embed/lHILbkfZ9e4');
        break;

    case '<li>Start</li><li>Faire</li><li>Sport</li><li>Intermédiaire</li><li>Avec équipement</li><li>Musculation</li><li>Bas du corps</li>':
        urlVideos.push('https://www.youtube.com/embed/k20aYCK1Ru4');
        urlVideos.push('https://www.youtube.com/embed/O6zJdV4nZ4U');
        urlVideos.push('https://www.youtube.com/embed/xPCDR5b9F70');
        break;

    case '<li>Start</li><li>Faire</li><li>Sport</li><li>Intermédiaire</li><li>Avec équipement</li><li>Musculation</li><li>Haut du corps</li>':
        urlVideos.push('https://www.youtube.com/embed/72gZihBC56c');
        urlVideos.push('https://www.youtube.com/embed/AzIkSv3mo-Y');
        urlVideos.push('https://www.youtube.com/embed/-16pyrNVoaE');
        break;

    case '<li>Start</li><li>Faire</li><li>Sport</li><li>Intermédiaire</li><li>Avec équipement</li><li>Perte de graisse</li>':
        urlVideos.push('https://www.youtube.com/embed/qk3MXOJ60kE');
        urlVideos.push('https://www.youtube.com/embed/WxEiJ4Yy8g4');
        urlVideos.push('https://www.youtube.com/embed/6EvdR5lFq48');
        break;

    case '<li>Start</li><li>Faire</li><li>Sport</li><li>Intermédiaire</li><li>Sans équipement</li><li>Perte de graisse</li>':
        urlVideos.push('https://www.youtube.com/embed/ku3vgejAZ0g');
        urlVideos.push('https://www.youtube.com/embed/7DXj_d1FrAQ');
        urlVideos.push('https://www.youtube.com/embed/wQvBZPdXUz4');
        break;

    case '<li>Start</li><li>Faire</li><li>Sport</li><li>Avancé</li><li>Avec équipement</li><li>Musculation</li><li>Bas du corps</li>':
        urlVideos.push('https://www.youtube.com/embed/ZtvFNaQcwkc');
        urlVideos.push('https://www.youtube.com/embed/dFnL8-D_QdI');
        urlVideos.push('https://www.youtube.com/embed/afUGTKsym_Y');
        break;

    case '<li>Start</li><li>Faire</li><li>Sport</li><li>Avancé</li><li>Avec équipement</li><li>Musculation</li><li>Haut du corps</li>':
        urlVideos.push('https://www.youtube.com/embed/ixKnTL8RFSI');
        urlVideos.push('https://www.youtube.com/embed/yPiANlQxank');
        urlVideos.push('https://www.youtube.com/embed/ynDyv6LDRTM');
        break;

    case '<li>Start</li><li>Faire</li><li>Sport</li><li>Avancé</li><li>Avec équipement</li><li>Perte de graisse</li>':
        urlVideos.push('https://www.youtube.com/embed/vVZ--4TTD7k');
        urlVideos.push('https://www.youtube.com/embed/cuHwoCWFLIw');
        urlVideos.push('https://www.youtube.com/embed/NDvEySqPSio');
        break;

    case '<li>Start</li><li>Faire</li><li>Sport</li><li>Avancé</li><li>Sans équipement</li><li>Perte de graisse</li>':
        urlVideos.push('https://www.youtube.com/embed/NDvEySqPSio');
        urlVideos.push('https://www.youtube.com/embed/_Zem0_qsDg0');
        urlVideos.push('https://www.youtube.com/embed/CKUAeaBWu18');
        urlVideos.push('https://www.youtube.com/embed/VUpGLzYJJNM');
        break;

    case '<li>Start</li><li>Faire</li><li>Sport</li><li>Débutant</li><li>Sans équipement</li><li>Musculation</li><li>Bas du corps</li>':
        urlVideos.push('https://www.youtube.com/embed/H4hlvb3MCyw');
        urlVideos.push('https://www.youtube.com/embed/jE57-u2wRC8');
        urlVideos.push('https://www.youtube.com/embed/htL4XYk20Mc');
        break;

    case '<li>Start</li><li>Faire</li><li>Sport</li><li>Débutant</li><li>Sans équipement</li><li>Musculation</li><li>Haut du corps</li>':
        urlVideos.push('https://www.youtube.com/embed/BMLhJvhWZ0E');
        urlVideos.push('https://www.youtube.com/embed/avyAVV2Kd8k');
        urlVideos.push('https://www.youtube.com/embed/VUTn5-tV4Ak');
        break;

    case '<li>Start</li><li>Faire</li><li>Sport</li><li>Intermédiaire</li><li>Sans équipement</li><li>Musculation</li><li>Bas du corps</li>':
        urlVideos.push('https://www.youtube.com/embed/MjPVPDBJ94A');
        urlVideos.push('https://www.youtube.com/embed/9hQTvrP6EsM');
        urlVideos.push('https://www.youtube.com/embed/P5egxkKq5ew');
        break;

    case '<li>Start</li><li>Faire</li><li>Sport</li><li>Intermédiaire</li><li>Sans équipement</li><li>Musculation</li><li>Haut du corps</li>':
        urlVideos.push('https://www.youtube.com/embed/70CPGAn9STk');
        urlVideos.push('https://www.youtube.com/embed/c2KPDUYuBtM');
        urlVideos.push('https://www.youtube.com/embed/aI9VbuedMLI');
        break;

    case '<li>Start</li><li>Faire</li><li>Sport</li><li>Avancé</li><li>Sans équipement</li><li>Musculation</li><li>Bas du corps</li>':
        urlVideos.push('https://www.youtube.com/embed/NDvEySqPSio');
        urlVideos.push('https://www.youtube.com/embed/_j91KMBtYas');
        urlVideos.push('https://www.youtube.com/embed/hSgJd--UbKg');
        break;

    case '<li>Start</li><li>Faire</li><li>Sport</li><li>Avancé</li><li>Sans équipement</li><li>Musculation</li><li>Haut du corps</li>':
        urlVideos.push('https://www.youtube.com/embed/A61dLcsS9Ck');
        urlVideos.push('https://www.youtube.com/embed/uoqC6Voo8TU');
        urlVideos.push('https://www.youtube.com/embed/wRL_8yWrkvI');
        urlVideos.push('https://www.youtube.com/embed/Grr-c-ko01g');
        break;

    //SANTE
    case '<li>Start</li><li>Apprendre</li><li>Santé</li><li>Mentale</li><li>Addiction</li>':
        urlVideos.push('https://www.youtube.com/embed/NILRFyzSsPg');
        urlVideos.push('https://www.youtube.com/embed/3EB1Kxrsx6A');
        urlVideos.push('https://www.youtube.com/embed/DyK4vxbAmwQ');
        urlVideos.push('https://www.youtube.com/embed/th4nKyJxmt8');
        urlVideos.push('https://www.youtube.com/embed/6k1J_h4OxQg');
        break;

    case '<li>Start</li><li>Apprendre</li><li>Santé</li><li>Mentale</li><li>Dépression</li>':
        urlVideos.push('https://www.youtube.com/embed/KqAo0KHCyts');
        urlVideos.push('https://www.youtube.com/embed/J2JvaQHaSt8');
        urlVideos.push('https://www.youtube.com/embed/KMNXqj3QmxU');
        urlVideos.push('https://www.youtube.com/embed/6k1J_h4OxQg');
        break;

    case '<li>Start</li><li>Apprendre</li><li>Santé</li><li>Mentale</li><li>Trouble du comportement</li>':
        urlVideos.push('https://www.youtube.com/embed/rSW_kt9E3Qs');
        urlVideos.push('https://www.youtube.com/embed/d06EXH6U_Ng');
        urlVideos.push('https://www.youtube.com/embed/DVhcAZM8Oyg');
        urlVideos.push('https://www.youtube.com/embed/IcRLBYhFYhI');
        break;

    case '<li>Start</li><li>Apprendre</li><li>Santé</li><li>Mentale</li><li>Schizophrénie</li>':
        urlVideos.push('https://www.youtube.com/embed/QkpUkbFlHy8');
        urlVideos.push('https://www.youtube.com/embed/rGuwoKTjhBo');
        urlVideos.push('https://www.youtube.com/embed/OGJN-F6BZ8A');
        urlVideos.push('https://www.youtube.com/embed/-Dz4cZmU8f8');
        break;

    case '<li>Start</li><li>Apprendre</li><li>Santé</li><li>Mentale</li><li>Bipolarité</li>':
        urlVideos.push('https://www.youtube.com/embed/4BHhry5F564');
        urlVideos.push('https://www.youtube.com/embed/l8nmYMwd4FU');
        urlVideos.push('https://www.youtube.com/embed/rsfuR-uDeM0');
        break;

    case '<li>Start</li><li>Apprendre</li><li>Santé</li><li>Physique</li><li>Système</li><li>Respiratoire</li>':
        urlVideos.push('https://www.youtube.com/embed/drSiOlmp7N4');
        urlVideos.push('https://www.youtube.com/embed/HoZss4Sc1E0');
        urlVideos.push('https://www.youtube.com/embed/tYTbbPSGIHk');
        break; 
            
    case '<li>Start</li><li>Apprendre</li><li>Santé</li><li>Physique</li><li>Système</li><li>Digestif</li>':
        urlVideos.push('https://www.youtube.com/embed/LgHgOxtonmM');
        urlVideos.push('https://www.youtube.com/embed/AnmHhWsGQdA');
        urlVideos.push('https://www.youtube.com/embed/brEKIsETGfw');
        break;

    case '<li>Start</li><li>Apprendre</li><li>Santé</li><li>Physique</li><li>Système</li><li>Circulatoire</li>':
        urlVideos.push('https://www.youtube.com/embed/s7SuTXiGupQ');
        urlVideos.push('https://www.youtube.com/embed/rBZyKYoh2BU');
        urlVideos.push('https://www.youtube.com/embed/u7qZAs5lhBs');
        break;

    case '<li>Start</li><li>Apprendre</li><li>Santé</li><li>Physique</li><li>Système</li><li>Nerveux</li>':
        urlVideos.push('https://www.youtube.com/embed/891NUdFbB04');
        urlVideos.push('https://www.youtube.com/embed/hXFBWXuZIdo');
        urlVideos.push('https://www.youtube.com/embed/iNSG9O_Iw2w');
        break;

    case '<li>Start</li><li>Apprendre</li><li>Santé</li><li>Physique</li><li>Système</li><li>Musculaire/Squelettique</li>':
        urlVideos.push('https://www.youtube.com/embed/IzbcwsHy4v8');
        urlVideos.push('https://www.youtube.com/embed/REWwSmnYuM0');
        urlVideos.push('https://www.youtube.com/embed/xyQ-UGWQWe8');
        break;

    case '<li>Start</li><li>Apprendre</li><li>Santé</li><li>Physique</li><li>Maladie</li><li>Cancer</li>':
        urlVideos.push('https://www.youtube.com/embed/xoayhLS2-eM');
        urlVideos.push('https://www.youtube.com/embed/5Acsnl1gEzI');
        urlVideos.push('https://www.youtube.com/embed/YUK131ZKPtk');
        break;

    case '<li>Start</li><li>Apprendre</li><li>Santé</li><li>Physique</li><li>Maladie</li><li>Alzheimer</li>':
        urlVideos.push('https://www.youtube.com/embed/0PNjvwEInYM');
        urlVideos.push('https://www.youtube.com/embed/MK2Y3jDZr4I');
        urlVideos.push('https://www.youtube.com/embed/tUGsrTirpAg');
        break;

    case '<li>Start</li><li>Apprendre</li><li>Santé</li><li>Physique</li><li>Maladie</li><li>Parkinson</li>':
        urlVideos.push('https://www.youtube.com/embed/FCa3K4d6KL0');
        urlVideos.push('https://www.youtube.com/embed/EC2IAwxCe-Q');
        urlVideos.push('https://www.youtube.com/embed/-XkdsBg7nfc');
        break;

    case '<li>Start</li><li>Apprendre</li><li>Santé</li><li>Physique</li><li>Maladie</li><li>Sida</li>':
        urlVideos.push('https://www.youtube.com/embed/KKJInAkxnLY');
        urlVideos.push('https://www.youtube.com/embed/qWQptSNZemM');
        urlVideos.push('https://www.youtube.com/embed/ujZkefAlV_E');
        break;

    case '<li>Start</li><li>Apprendre</li><li>Santé</li><li>Physique</li><li>Maladie</li><li>Grippe</li>':
        urlVideos.push('https://www.youtube.com/embed/8mo8rWWJZkc');
        urlVideos.push('https://www.youtube.com/embed/bM7AOBxqjnE');
        urlVideos.push('https://www.youtube.com/embed/R8tFxpRYy6c');
        break;

    //HISTOIRE
    case '<li>Start</li><li>Apprendre</li><li>Histoire</li><li>Epoque</li><li>Préhistoire</li>':
        urlVideos.push('https://www.youtube.com/embed/BCfz0K36j9g');
        urlVideos.push('https://www.youtube.com/embed/bX5zXfrZ3zY');
        urlVideos.push('https://www.youtube.com/embed/RhVPXEZ_HkQ');
        break;

    case '<li>Start</li><li>Apprendre</li><li>Histoire</li><li>Epoque</li><li>Antiquité</li>':
        urlVideos.push('https://www.youtube.com/embed/NdTXF94t7J8');
        urlVideos.push('https://www.youtube.com/embed/2djs68Vh13U');
        urlVideos.push('https://www.youtube.com/embed/-mLpXnL9dEc');
        urlVideos.push('https://www.youtube.com/embed/W_dJi0_PG1w');
        urlVideos.push('https://www.youtube.com/embed/JzqoBaaT3GA');
        urlVideos.push('https://www.youtube.com/embed/RUAS6kDtfkk');
        break;
    
    case '<li>Start</li><li>Apprendre</li><li>Histoire</li><li>Epoque</li><li>Moyen-âge</li>':
        urlVideos.push('https://www.youtube.com/embed/QXfr-5s3exc');
        urlVideos.push('https://www.youtube.com/embed/356sUghcSZg');
        urlVideos.push('https://www.youtube.com/embed/JLu6lS2bEQM');
        urlVideos.push('https://www.youtube.com/embed/87zpYe0x0W8');
        urlVideos.push('https://www.youtube.com/embed/fbKCnLt1o-I');
        urlVideos.push('https://www.youtube.com/embed/jrvihfN1PGE');
        break;

    case '<li>Start</li><li>Apprendre</li><li>Histoire</li><li>Epoque</li><li>Moderne</li>':
        urlVideos.push('https://www.youtube.com/embed/6vTTo47xfds');
        urlVideos.push('https://www.youtube.com/embed/izQ3j7XQPP4');
        urlVideos.push('https://www.youtube.com/embed/lQkbV81jb14');
        urlVideos.push('https://www.youtube.com/embed/Gm4WPFoOMfA');
        urlVideos.push('https://www.youtube.com/embed/XHdyZs_2VFE');
        urlVideos.push('https://www.youtube.com/embed/7AaejeFJ5-U');
        break;

    case '<li>Start</li><li>Apprendre</li><li>Histoire</li><li>Epoque</li><li>Contemporaine</li>':
        urlVideos.push('https://www.youtube.com/embed/VuYftrvJN9o');
        urlVideos.push('https://www.youtube.com/embed/41u5XEpr4q4');
        urlVideos.push('https://www.youtube.com/embed/Jf5b42Llujc');
        urlVideos.push('https://www.youtube.com/embed/CmWBMw-9lY4');
        urlVideos.push('https://www.youtube.com/embed/kdyA9u5Gszc');
        urlVideos.push('https://www.youtube.com/embed/o5F8EVzJ4co');
        break;
    
    case '<li>Start</li><li>Apprendre</li><li>Histoire</li><li>Grands événements</li><li>Guerres</li><li>Sept ans</li>':
        urlVideos.push('https://www.youtube.com/embed/N01db5Y-3B8');
        urlVideos.push('https://www.youtube.com/embed/GXP_K03vWFM');
        break;

    case '<li>Start</li><li>Apprendre</li><li>Histoire</li><li>Grands événements</li><li>Guerres</li><li>Première guerre</li>':
        urlVideos.push('https://www.youtube.com/embed/UB1CpigwnGs');
        urlVideos.push('https://www.youtube.com/embed/5B0pE1eCxGU');
        urlVideos.push('https://www.youtube.com/embed/nsB7mM7-o3A');
        urlVideos.push('https://www.youtube.com/embed/zOgo6bbKGcw');
        urlVideos.push('https://www.youtube.com/embed/EEDOvOx28_M');
        break;

    case '<li>Start</li><li>Apprendre</li><li>Histoire</li><li>Grands événements</li><li>Guerres</li><li>Deuxième guerre</li>':
        urlVideos.push('https://www.youtube.com/embed/aYFNuwShrhM');
        urlVideos.push('https://www.youtube.com/embed/n_bgNMbnQ2o');
        urlVideos.push('https://www.youtube.com/embed/SRweS3czci8');
        urlVideos.push('https://www.youtube.com/embed/aGSquA3tIRY');
        urlVideos.push('https://www.youtube.com/embed/9sxjd7hk1Zw');
        break;

    case '<li>Start</li><li>Apprendre</li><li>Histoire</li><li>Grands événements</li><li>Guerres</li><li>Guerre froide</li>':
        urlVideos.push('https://www.youtube.com/embed/OdWs1EL1WN4');
        urlVideos.push('https://www.youtube.com/embed/MhfraAqwZD0');
        urlVideos.push('https://www.youtube.com/embed/vYuj97AlsFo');
        break;

    case '<li>Start</li><li>Apprendre</li><li>Histoire</li><li>Grands événements</li><li>Découvertes</li><li>Monde</li>':
        urlVideos.push('https://www.youtube.com/embed/poRLL6s7ZTc');
        urlVideos.push('https://www.youtube.com/embed/T--268qZCY8');
        urlVideos.push('https://www.youtube.com/embed/J_4RlrGerEg');
        urlVideos.push('https://www.youtube.com/embed/c3zFYUlYR9k');
        break;
    
    case '<li>Start</li><li>Apprendre</li><li>Histoire</li><li>Grands événements</li><li>Découvertes</li><li>Inventions</li><li>Thomas Edison</li>':
        urlVideos.push('https://www.youtube.com/embed/rKF1PLguCjM');
        urlVideos.push('https://www.youtube.com/embed/zmlFAmjQGjQ');
        urlVideos.push('https://www.youtube.com/embed/XvYJKfcbrWA');
        break;
    
    case '<li>Start</li><li>Apprendre</li><li>Histoire</li><li>Grands événements</li><li>Découvertes</li><li>Inventions</li><li>Nikola Tesla</li>':
        urlVideos.push('https://www.youtube.com/embed/-Vaylhlhv3U');
        urlVideos.push('https://www.youtube.com/embed/nKFXoqVMoSQ');
        urlVideos.push('https://www.youtube.com/embed/mxpmu4pdj8Q');
        break;

    case '<li>Start</li><li>Apprendre</li><li>Histoire</li><li>Grands événements</li><li>Découvertes</li><li>Inventions</li><li>Albert Einstein</li>':
        urlVideos.push('https://www.youtube.com/embed/YVYSFl4M5B8');
        urlVideos.push('https://www.youtube.com/embed/Ll3mDQx07Yg');
        urlVideos.push('https://www.youtube.com/embed/M02c1zp0f0w');
        urlVideos.push('https://www.youtube.com/embed/ZCFG3Jx3tIU');
        break;
    
    case '<li>Start</li><li>Apprendre</li><li>Histoire</li><li>Grands événements</li><li>Découvertes</li><li>Inventions</li><li>Isaac Newton</li>':
        urlVideos.push('https://www.youtube.com/embed/0cpV4T8r6rc');
        urlVideos.push('https://www.youtube.com/embed/xACFN-GiDaA');
        urlVideos.push('https://www.youtube.com/embed/224e9Ykwta8');
        break;
        
    case '<li>Start</li><li>Apprendre</li><li>Histoire</li><li>Grands événements</li><li>Découvertes</li><li>Inventions</li><li>Marie Curie</li>':
        urlVideos.push('https://www.youtube.com/embed/jlnc4vz-vDg');
        urlVideos.push('https://www.youtube.com/embed/lKfhVLqadDQ');
        urlVideos.push('https://www.youtube.com/embed/strm56YCNio');
        break;

    case '<li>Start</li><li>Apprendre</li><li>Histoire</li><li>Grands événements</li><li>Empire Romain</li>':
        urlVideos.push('https://www.youtube.com/embed/Y0ZqYwf1aj4');
        urlVideos.push('https://www.youtube.com/embed/TJWL0fQg7as');
        urlVideos.push('https://www.youtube.com/embed/_0qhMjTCYOk');
        break;
        
    case '<li>Start</li><li>Apprendre</li><li>Histoire</li><li>Grands événements</li><li>Femmes</li>':
        urlVideos.push('https://www.youtube.com/embed/C3THYevbKNE');
        urlVideos.push('https://www.youtube.com/embed/BfW72FjVC6k');
        urlVideos.push('https://www.youtube.com/embed/sT9gtc98drE');
        urlVideos.push('https://www.youtube.com/embed/ksbVRRoszeU');        
        urlVideos.push('https://www.youtube.com/embed/20FmhTjhgr8');
        break;
    
    case '<li>Start</li><li>Apprendre</li><li>Histoire</li><li>Grands événements</li><li>Lune</li>':
        urlVideos.push('https://www.youtube.com/embed/yuBxzyndcDs');
        urlVideos.push('https://www.youtube.com/embed/QGTKpmsNAGw');
        urlVideos.push('https://www.youtube.com/embed/iR3oXFFISI0');
        urlVideos.push('https://www.youtube.com/embed/JEWVTWk9Wec');
        break;

    //CUISINE
    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Salé</li><li>équilibré</li><li>Vegan</li><li>Asiatique</li>':
        urlVideos.push('https://www.youtube.com/embed/H4bAVqruiUo');
        urlVideos.push('https://www.youtube.com/embed/hKoYeR0dvkU');
        urlVideos.push('https://www.youtube.com/embed/w9mGmC_hDH0');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Salé</li><li>équilibré</li><li>Vegan</li><li>Européen</li>':
        urlVideos.push('https://www.youtube.com/embed/oJHNrC_p8hQ');
        urlVideos.push('https://www.youtube.com/embed/s_WKsDI5ckQ');
        urlVideos.push('https://www.youtube.com/embed/Sz5b89pwY_Y');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Salé</li><li>équilibré</li><li>Vegan</li><li>Américain</li>':
        urlVideos.push('https://www.youtube.com/embed/MIihsRc8Bi8');
        urlVideos.push('https://www.youtube.com/embed/0gwSaBuxLiM');
        urlVideos.push('https://www.youtube.com/embed/-MuRx3ffS-Y');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Salé</li><li>équilibré</li><li>Vegan</li><li>Africain</li>':
        urlVideos.push('https://www.youtube.com/embed/JZ2V7fIgZzQ');
        urlVideos.push('https://www.youtube.com/embed/AR4739kePOQ');
        urlVideos.push('https://www.youtube.com/embed/b1M3q4G98Ko');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Salé</li><li>équilibré</li><li>Omnivore</li><li>Asiatique</li>':
        urlVideos.push('https://www.youtube.com/embed/2_nR3KiOyNU');
        urlVideos.push('https://www.youtube.com/embed/fmJ9bV9XvUY');
        urlVideos.push('https://www.youtube.com/embed/73yQWGbVIak');
        urlVideos.push('https://www.youtube.com/embed/u4J9gV4IzFM');
        urlVideos.push('https://www.youtube.com/embed/s1KM13AhRF4');
        urlVideos.push('https://www.youtube.com/embed/kVILxACYGig');
        urlVideos.push('https://www.youtube.com/embed/7_ot_Nlv7ac');
        urlVideos.push('https://www.youtube.com/embed/1L0EhIpynXQ');
        urlVideos.push('https://www.youtube.com/embed/Y9z2-TemPJk');
        urlVideos.push('https://www.youtube.com/embed/oYDEhT9v6sY');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Salé</li><li>équilibré</li><li>Omnivore</li><li>Européen</li>':
        urlVideos.push('https://www.youtube.com/embed/ogAnvfxDNfw');
        urlVideos.push('https://www.youtube.com/embed/oYIIoxiDjuk');
        urlVideos.push('https://www.youtube.com/embed/X3kk-kcs6VA');
        urlVideos.push('https://www.youtube.com/embed/SGC6lLNhNsY');
        urlVideos.push('https://www.youtube.com/embed/MTPVsSLah3E');
        urlVideos.push('https://www.youtube.com/embed/6_EhqGnTLOg');
        urlVideos.push('https://www.youtube.com/embed/QSJ3Zde0x7g');
        urlVideos.push('https://www.youtube.com/embed/-3cvWUUCY7o');
        urlVideos.push('https://www.youtube.com/embed/ZDUs14mw8sQ');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Salé</li><li>équilibré</li><li>Omnivore</li><li>Américain</li>':
        urlVideos.push('https://www.youtube.com/embed/v-TtVvilah4');
        urlVideos.push('https://www.youtube.com/embed/fLvFP6h2qHU');
        urlVideos.push('https://www.youtube.com/embed/lgZQA8cIiUY');
        urlVideos.push('https://www.youtube.com/embed/yQlVJAx5kBk');
        urlVideos.push('https://www.youtube.com/embed/x1rvbXpAQeQ');
        break;
    
        case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Salé</li><li>équilibré</li><li>Omnivore</li><li>Africain</li>':
        urlVideos.push('https://www.youtube.com/embed/37w3QumGGss');
        urlVideos.push('https://www.youtube.com/embed/uJ3HZ_Q2Abo');
        urlVideos.push('https://www.youtube.com/embed/30G0PvPcjcY');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Salé</li><li>Gourmand</li><li>Omnivore</li><li>Asiatique</li>':
        urlVideos.push('https://www.youtube.com/embed/Vn0NL01RNWc');
        urlVideos.push('https://www.youtube.com/embed/Nq3e4vbpagQ');
        urlVideos.push('https://www.youtube.com/embed/Xc3HptgyuCs');
        urlVideos.push('https://www.youtube.com/embed/-2df-QroXyw');
        urlVideos.push('https://www.youtube.com/embed/PlFChSVy-F4');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Salé</li><li>Gourmand</li><li>Omnivore</li><li>Européen</li>':
        urlVideos.push('https://www.youtube.com/embed/kSb62MGJSI4');
        urlVideos.push('https://www.youtube.com/embed/wkbYh1z82b4');
        urlVideos.push('https://www.youtube.com/embed/VdbJqMd0CX0');
        urlVideos.push('https://www.youtube.com/embed/aYVinxtGlnI');
        urlVideos.push('https://www.youtube.com/embed/aPde98NbJ0s');
        urlVideos.push('https://www.youtube.com/embed/9FdOab4trX0');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Salé</li><li>Gourmand</li><li>Omnivore</li><li>Américain</li>':
        urlVideos.push('https://www.youtube.com/embed/hoh2x3EAMuE');
        urlVideos.push('https://www.youtube.com/embed/3VwPMJmnrsY');
        urlVideos.push('https://www.youtube.com/embed/nMmCxk81LQc');
        urlVideos.push('https://www.youtube.com/embed/TN-ZsfOBMmw');
        urlVideos.push('https://www.youtube.com/embed/aG-I-VPqHug');
        urlVideos.push('https://www.youtube.com/embed/Uwg-yqGPi8Y');
        break;    
        
    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Salé</li><li>Gourmand</li><li>Omnivore</li><li>Africain</li>':
        urlVideos.push('https://www.youtube.com/embed/s5eb1_RlJNg');
        urlVideos.push('https://www.youtube.com/embed/O_YBtz6aHDE');
        urlVideos.push('https://www.youtube.com/embed/lo0PcLJ439Y');
        urlVideos.push('https://www.youtube.com/embed/SETOMKyHKeE');
        urlVideos.push('https://www.youtube.com/embed/UZ5GnRKrjA8');
        urlVideos.push('https://www.youtube.com/embed/YivlnQADjJI');
        break;    
        
    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Salé</li><li>Gourmand</li><li>Vegan</li><li>Asiatique</li>':
        urlVideos.push('https://www.youtube.com/embed/KQlbPyevgUI');
        urlVideos.push('https://www.youtube.com/embed/nUH7Iv8qWBo');
        urlVideos.push('https://www.youtube.com/embed/tVTsOZyZ9LE');
        urlVideos.push('https://www.youtube.com/embed/fZcHwE5ommc');
        urlVideos.push('https://www.youtube.com/embed/QO1EWD205cY');
        urlVideos.push('https://www.youtube.com/embed/XrWZXBQVLqA');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Salé</li><li>Gourmand</li><li>Vegan</li><li>Européen</li>':
        urlVideos.push('https://www.youtube.com/embed/aFRGqc5b_EM');
        urlVideos.push('https://www.youtube.com/embed/k0xkrtGp2vM');
        urlVideos.push('https://www.youtube.com/embed/8CSb4x3MWhc');
        urlVideos.push('https://www.youtube.com/embed/QJ-u3ez9WvQ');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Salé</li><li>Gourmand</li><li>Vegan</li><li>Américain</li>':
        urlVideos.push('https://www.youtube.com/embed/_pkli1AueYQ');
        urlVideos.push('https://www.youtube.com/embed/FhJ6QMUKIRc');
        urlVideos.push('https://www.youtube.com/embed/XTuYq_XcnW8');
        urlVideos.push('https://www.youtube.com/embed/meQiq_WX9wE');
        urlVideos.push('https://www.youtube.com/embed/0bOCGfk4rCs');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Salé</li><li>Gourmand</li><li>Vegan</li><li>Africain</li>':
        urlVideos.push('https://www.youtube.com/embed/IA9MZMXde14');
        urlVideos.push('https://www.youtube.com/embed/QaYn5Zf39hU');
        urlVideos.push('https://www.youtube.com/embed/Sv1luldeNz4');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Sucré</li><li>équilibré</li><li>Vegan</li><li>Asiatique</li>':
        urlVideos.push('https://www.youtube.com/embed/UCOe6lAKJ58');
        urlVideos.push('https://www.youtube.com/embed/SS_B77XgV4I');
        urlVideos.push('https://www.youtube.com/embed/lei4Hgd_bPA');
        urlVideos.push('https://www.youtube.com/embed/b6KBkzvWZrY');
        urlVideos.push('https://www.youtube.com/embed/1ycCh8MGLDo');
        urlVideos.push('https://www.youtube.com/embed/1TxABjPTZ2A');
        break;
        
    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Sucré</li><li>équilibré</li><li>Vegan</li><li>Européen</li>':
        urlVideos.push('https://www.youtube.com/embed/YNXXCoIEXZw');
        urlVideos.push('https://www.youtube.com/embed/oPMvr6q8wXU');
        urlVideos.push('https://www.youtube.com/embed/VKvXSD0Vcjo');
        break;
               
    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Sucré</li><li>équilibré</li><li>Vegan</li><li>Américain</li>':
        urlVideos.push('https://www.youtube.com/embed/dAJxUggNRts');
        urlVideos.push('https://www.youtube.com/embed/3X2qRzRs0RI');
        urlVideos.push('https://www.youtube.com/embed/ig2IpravYjU');
        urlVideos.push('https://www.youtube.com/embed/znaW5QZIwok');
        urlVideos.push('https://www.youtube.com/embed/y5kIkPaIyvc');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Sucré</li><li>équilibré</li><li>Vegan</li><li>Africain</li>':
        urlVideos.push('https://www.youtube.com/embed/KgMyC5fzwjE');
        urlVideos.push('https://www.youtube.com/embed/xU2OoJC2US0');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Sucré</li><li>équilibré</li><li>Omnivore</li><li>Asiatique</li>':
        urlVideos.push('https://www.youtube.com/embed/Y5uE9yvmJTo');
        urlVideos.push('https://www.youtube.com/embed/bDWNYYNcOck');
        urlVideos.push('https://www.youtube.com/embed/Mgd7qaoLcXs');
        urlVideos.push('https://www.youtube.com/embed/SdGR_6RHynE');
        urlVideos.push('https://www.youtube.com/embed/5AGBYFktWUw');
        break;


    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Sucré</li><li>équilibré</li><li>Omnivore</li><li>Européen</li>':
        urlVideos.push('https://www.youtube.com/embed/xGLJw086Aek');
        urlVideos.push('https://www.youtube.com/embed/tEhxGPjrbRM');
        urlVideos.push('https://www.youtube.com/embed/gj4OOfsqzjM');
        urlVideos.push('https://www.youtube.com/embed/YNO8WHDWZh4');
        urlVideos.push('https://www.youtube.com/embed/ZwNQ0bMGNlA');
        urlVideos.push('https://www.youtube.com/embed/WfJDKMLd5Q4');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Sucré</li><li>équilibré</li><li>Omnivore</li><li>Américain</li>':
        urlVideos.push('https://www.youtube.com/embed/yA_sxOMIUb');
        urlVideos.push('https://www.youtube.com/embed/upR1mw1QAxg');
        urlVideos.push('https://www.youtube.com/embed/z1i_4DgZZW8');
        urlVideos.push('https://www.youtube.com/embed/t-cpw63hjfc');
        urlVideos.push('https://www.youtube.com/embed/WoRdf-iDKwg');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Sucré</li><li>équilibré</li><li>Omnivore</li><li>Africain</li>':
        urlVideos.push('https://www.youtube.com/embed/SV2vzgHywGs');
        urlVideos.push('https://www.youtube.com/embed/7WvcpXrvuW0');
        urlVideos.push('https://www.youtube.com/embed/dCwmsMZMsn4');
        urlVideos.push('https://www.youtube.com/embed/SV2vzgHywGs');
        urlVideos.push('https://www.youtube.com/embed/KFtzALdLgfE');
        urlVideos.push('https://www.youtube.com/embed/dCwmsMZMsn4');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Sucré</li><li>Gourmand</li><li>Omnivore</li><li>Asiatique</li>':
        urlVideos.push('https://www.youtube.com/embed/X4TxIZ4AgIg');
        urlVideos.push('https://www.youtube.com/embed/9PyH5fZp_z0');
        urlVideos.push('https://www.youtube.com/embed/o32j0lspqtg');
        urlVideos.push('https://www.youtube.com/embed/2A9ztgNDFDo');
        urlVideos.push('https://www.youtube.com/embed/lv7EAHMS6e0');
        urlVideos.push('https://www.youtube.com/embed/ooKSlkxkn_E');
        break;
    
    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Sucré</li><li>Gourmand</li><li>Omnivore</li><li>Européen</li>':
        urlVideos.push('https://www.youtube.com/embed/wmEyelPo4Uc');
        urlVideos.push('https://www.youtube.com/embed/2u3SPeKk4FU');
        urlVideos.push('https://www.youtube.com/embed/fwL36Qxux2E');
        urlVideos.push('https://www.youtube.com/embed/SISGaPCOd78');
        urlVideos.push('https://www.youtube.com/embed/QyfmHFrY9Hk');
        urlVideos.push('https://www.youtube.com/embed/oid48haiDDM');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Sucré</li><li>Gourmand</li><li>Omnivore</li><li>Américain</li>':
        urlVideos.push('https://www.youtube.com/embed/anPqwXn1rzA');
        urlVideos.push('https://www.youtube.com/embed/4jqgGvk3d8Y');
        urlVideos.push('https://www.youtube.com/embed/869cFCo3eK4');
        urlVideos.push('https://www.youtube.com/embed/g-OPcLZNQcg');
        urlVideos.push('https://www.youtube.com/embed/Nx844igAeXg');
        urlVideos.push('https://www.youtube.com/embed/zW97wlMzHbo');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Sucré</li><li>Gourmand</li><li>Omnivore</li><li>Africain</li>':
        urlVideos.push('https://www.youtube.com/embed/d_IjqCJoLRE');
        urlVideos.push('https://www.youtube.com/embed/fzDe6QfV3sk');
        urlVideos.push('https://www.youtube.com/embed/uQf1D7RkS50');
        urlVideos.push('https://www.youtube.com/embed/23WJj4u0rlQ');
        urlVideos.push('https://www.youtube.com/embed/Us88SsTGXW8');
        urlVideos.push('https://www.youtube.com/embed/L7d5uBRWayg');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Sucré</li><li>Gourmand</li><li>Vegan</li><li>Asiatique</li>':
        urlVideos.push('https://www.youtube.com/embed/OFHnGNZgCOc');
        urlVideos.push('https://www.youtube.com/embed/EeXHuYG01wU');
        urlVideos.push('https://www.youtube.com/embed/GV1T7TMZK6c');
        urlVideos.push('https://www.youtube.com/embed/PmRGcEo7jtc');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Sucré</li><li>Gourmand</li><li>Vegan</li><li>Européen</li>':
        urlVideos.push('https://www.youtube.com/embed/YpRmZWB4_KQ');
        urlVideos.push('https://www.youtube.com/embed/hmVFwSDrLoc');
        urlVideos.push('https://www.youtube.com/embed/ocEr9Hw3C_I');
        urlVideos.push('https://www.youtube.com/embed/w9JdSDXD6v8');
        urlVideos.push('https://www.youtube.com/embed/Tv5BgfXm8No');
        urlVideos.push('https://www.youtube.com/embed/vjAtt54EUbU');
        break;    
        
    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Sucré</li><li>Gourmand</li><li>Vegan</li><li>Américain</li>':
        urlVideos.push('https://www.youtube.com/embed/nN2l4pcbtv0');
        urlVideos.push('https://www.youtube.com/embed/Vf5mfMKXRyE');
        urlVideos.push('https://www.youtube.com/embed/-sHzwq4T1LU');
        urlVideos.push('https://www.youtube.com/embed/pA5ve9_DxjY');
        urlVideos.push('https://www.youtube.com/embed/0gRWNsQF5nA');
        urlVideos.push('https://www.youtube.com/embed/aPuIOXgG5uU');
        break;
                    
    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>élaboré</li><li>Sucré</li><li>Gourmand</li><li>Vegan</li><li>Africain</li>':
        urlVideos.push('https://www.youtube.com/embed/KgMyC5fzwjE');
        urlVideos.push('https://www.youtube.com/embed/xU2OoJC2US0');
        break;
          
    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Salé</li><li>équilibré</li><li>Vegan</li><li>Asiatique</li>':
        urlVideos.push('https://www.youtube.com/embed/LnI_LTfU9ZI');
        urlVideos.push('https://www.youtube.com/embed/NqOtq3bW3dQ');
        urlVideos.push('https://www.youtube.com/embed/H4bAVqruiUo');
        urlVideos.push('https://www.youtube.com/embed/hKoYeR0dvkU');
        urlVideos.push('https://www.youtube.com/embed/w9mGmC_hDH0');
        break; 

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Salé</li><li>équilibré</li><li>Vegan</li><li>Européen</li>':
        urlVideos.push('https://www.youtube.com/embed/NqOtq3bW3dQ');
        urlVideos.push('https://www.youtube.com/embed/oJHNrC_p8hQ');
        urlVideos.push('https://www.youtube.com/embed/s_WKsDI5ckQ');
        urlVideos.push('https://www.youtube.com/embed/Sz5b89pwY_Y');
        break;          

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Salé</li><li>équilibré</li><li>Vegan</li><li>Américain</li>':
        urlVideos.push('https://www.youtube.com/embed/BLSx-jk7DeM');
        urlVideos.push('https://www.youtube.com/embed/H5ZyknClXzc');
        urlVideos.push('https://www.youtube.com/embed/77XUQ7KjOBk');
        break;    

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Salé</li><li>équilibré</li><li>Vegan</li><li>Africain</li>':
        urlVideos.push('https://www.youtube.com/embed/SETOMKyHKeE');
        urlVideos.push('https://www.youtube.com/embed/yvNPutCmQEo');
        urlVideos.push('https://www.youtube.com/embed/ZDymNGYpl3w');
        urlVideos.push('https://www.youtube.com/embed/iJH4tTmBA9A');
        urlVideos.push('https://www.youtube.com/embed/I-hiWKvv6-I');
        urlVideos.push('https://www.youtube.com/embed/abG8-Je8IT8');
        break;      
    
    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Salé</li><li>équilibré</li><li>Omnivore</li><li>Asiatique</li>':
        urlVideos.push('https://www.youtube.com/embed/8rQ22YtUgcU');
        urlVideos.push('https://www.youtube.com/embed/gFG_7IA2bns');
        urlVideos.push('https://www.youtube.com/embed/tcCWnAkxS_Y');
        urlVideos.push('https://www.youtube.com/embed/u4J9gV4IzFM');
        urlVideos.push('https://www.youtube.com/embed/D6DbCD9T1Ls');
        urlVideos.push('https://www.youtube.com/embed/x0oA63U9ptk');
        break;      
    
    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Salé</li><li>équilibré</li><li>Omnivore</li><li>Européen</li>':
        urlVideos.push('https://www.youtube.com/embed/4R5Re02mDbE');
        urlVideos.push('https://www.youtube.com/embed/it-wSk5ctUU');
        urlVideos.push('https://www.youtube.com/embed/QTQPeMjoHXk');
        urlVideos.push('https://www.youtube.com/embed/zuROAfqsZzw');
        urlVideos.push('https://www.youtube.com/embed/YBBBaWxeQlU');
        urlVideos.push('https://www.youtube.com/embed/6_EhqGnTLOg');
        break;  
            
    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Salé</li><li>équilibré</li><li>Omnivore</li><li>Américain</li>':
        urlVideos.push('https://www.youtube.com/embed/M8OGXmLTTiI');
        urlVideos.push('https://www.youtube.com/embed/VdRYRWGwYCM');
        urlVideos.push('https://www.youtube.com/embed/X4r3GB3siOU');
        urlVideos.push('https://www.youtube.com/embed/YqsiWvUdoqs');
        urlVideos.push('https://www.youtube.com/embed/U1Evz1WlHWQ');
        urlVideos.push('https://www.youtube.com/embed/KmzBwppxons');
        break;  
            
    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Salé</li><li>équilibré</li><li>Omnivore</li><li>Africain</li>':
        urlVideos.push('https://www.youtube.com/embed/37w3QumGGss');
        urlVideos.push('https://www.youtube.com/embed/uJ3HZ_Q2Abo');
        urlVideos.push('https://www.youtube.com/embed/30G0PvPcjcY');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Salé</li><li>Gourmand</li><li>Omnivore</li><li>Asiatique</li>':
        urlVideos.push('https://www.youtube.com/embed/8vCOdIojork');
        urlVideos.push('https://www.youtube.com/embed/Dewcqv3Q3BA');
        urlVideos.push('https://www.youtube.com/embed/nHw6zhyLugg');
        urlVideos.push('https://www.youtube.com/embed/N3dAURdCMuk');
        urlVideos.push('https://www.youtube.com/embed/MmfnVWMjHzM');
        urlVideos.push('https://www.youtube.com/embed/FUAZitnHuhs');
        break;  
    
    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Salé</li><li>Gourmand</li><li>Omnivore</li><li>Européen</li>':
        urlVideos.push('https://www.youtube.com/embed/knCVKNGCbmY');
        urlVideos.push('https://www.youtube.com/embed/92Qnkn4NRnA');
        urlVideos.push('https://www.youtube.com/embed/WHMkwwsV_6Q');
        urlVideos.push('https://www.youtube.com/embed/mJF139VfVQc');
        urlVideos.push('https://www.youtube.com/embed/Nb3Hrvo_NGs');
        urlVideos.push('https://www.youtube.com/embed/8TkmBoeEB5Y');
        break;   

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Salé</li><li>Gourmand</li><li>Omnivore</li><li>Américain</li>':
        urlVideos.push('https://www.youtube.com/embed/hoh2x3EAMuE');
        urlVideos.push('https://www.youtube.com/embed/3VwPMJmnrsY');
        urlVideos.push('https://www.youtube.com/embed/nMmCxk81LQc');
        urlVideos.push('https://www.youtube.com/embed/S1DaJYOORMA');
        urlVideos.push('https://www.youtube.com/embed/mfIgYdJCwUA');
        urlVideos.push('https://www.youtube.com/embed/aG-I-VPqHug');
        break; 

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Salé</li><li>Gourmand</li><li>Omnivore</li><li>Africain</li>':
        urlVideos.push('https://www.youtube.com/embed/SETOMKyHKeE');
        urlVideos.push('https://www.youtube.com/embed/yvNPutCmQEo');
        urlVideos.push('https://www.youtube.com/embed/vAmUK_I0PUw');
        urlVideos.push('https://www.youtube.com/embed/xiOF52oEMTk');
        urlVideos.push('https://www.youtube.com/embed/UZ5GnRKrjA8');
        urlVideos.push('https://www.youtube.com/embed/06CpOKOWSys');
        break;      
        
    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Salé</li><li>Gourmand</li><li>Vegan</li><li>Asiatique</li>':
        urlVideos.push('https://www.youtube.com/embed/Fo9FwrSwSLw');
        urlVideos.push('https://www.youtube.com/embed/1kCTndfsoWU');
        urlVideos.push('https://www.youtube.com/embed/6e8H_eakM5Y');
        urlVideos.push('https://www.youtube.com/embed/14iSS-5UFgw');
        urlVideos.push('https://www.youtube.com/embed/nFGK6F9cYgk');
        urlVideos.push('https://www.youtube.com/embed/fIX9pPyaeic');
        break;      
        
    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Salé</li><li>Gourmand</li><li>Vegan</li><li>Européen</li>':
        urlVideos.push('https://www.youtube.com/embed/7Z7SYDbax6Q');
        urlVideos.push('https://www.youtube.com/embed/6RYXBnjH5tY');
        urlVideos.push('https://www.youtube.com/embed/TcJoUZAtITI');
        urlVideos.push('https://www.youtube.com/embed/nF51ZJxSFhw');
        urlVideos.push('https://www.youtube.com/embed/JOznUZd4TN4');
        urlVideos.push('https://www.youtube.com/embed/WmRN_5jb0ZU');
        break;  
        
    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Salé</li><li>Gourmand</li><li>Vegan</li><li>Américain</li>':
        urlVideos.push('https://www.youtube.com/embed/iSfguccIHqw');
        urlVideos.push('https://www.youtube.com/embed/wR2g3jeoLCA');
        urlVideos.push('https://www.youtube.com/embed/z7h1PLEPTec');
        urlVideos.push('https://www.youtube.com/embed/SekGtMd9_Bw');
        urlVideos.push('https://www.youtube.com/embed/WLPVHnQo7xg');
        urlVideos.push('https://www.youtube.com/embed/6aczxxDkZtU');
        break;  

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Salé</li><li>Gourmand</li><li>Vegan</li><li>Africain</li>':
        urlVideos.push('https://www.youtube.com/embed/SETOMKyHKeE');
        urlVideos.push('https://www.youtube.com/embed/yvNPutCmQEo');
        urlVideos.push('https://www.youtube.com/embed/ZDymNGYpl3w');
        urlVideos.push('https://www.youtube.com/embed/iJH4tTmBA9A');
        urlVideos.push('https://www.youtube.com/embed/NqOtq3bW3dQ');
        urlVideos.push('https://www.youtube.com/embed/37w3QumGGss');
        break;  

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Sucré</li><li>équilibré</li><li>Vegan</li><li>Asiatique</li>':
        urlVideos.push('https://www.youtube.com/embed/oPku8HakJwU');
        urlVideos.push('https://www.youtube.com/embed/Q3bZlFovgO4');
        urlVideos.push('https://www.youtube.com/embed/BVxnreSSmx4');
        urlVideos.push('https://www.youtube.com/embed/JWlV5nF8hzE');
        urlVideos.push('https://www.youtube.com/embed/5I3rngz6gQo');
        urlVideos.push('https://www.youtube.com/embed/UdOpSDiBtyM');
        break;  

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Sucré</li><li>équilibré</li><li>Vegan</li><li>Européen</li>':
        urlVideos.push('https://www.youtube.com/embed/qJTVep49EWo');
        urlVideos.push('https://www.youtube.com/embed/lMBhiR5TvNc');
        urlVideos.push('https://www.youtube.com/embed/-0IndrluwvM');
        urlVideos.push('https://www.youtube.com/embed/-VW2EPvkZRw');
        urlVideos.push('https://www.youtube.com/embed/Wz3MZYWPcvI');
        urlVideos.push('https://www.youtube.com/embed/VYEqGg1emPc');
        break;  

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Sucré</li><li>équilibré</li><li>Vegan</li><li>Américain</li>':
        urlVideos.push('https://www.youtube.com/embed/FtqPdIkjhvU');
        urlVideos.push('https://www.youtube.com/embed/Hn_eSwhqj0A');
        urlVideos.push('https://www.youtube.com/embed/fXLYqqxB2wc');
        urlVideos.push('https://www.youtube.com/embed/ZkG1WICdVwU');
        urlVideos.push('https://www.youtube.com/embed/6hOWu0Skfmw');
        urlVideos.push('https://www.youtube.com/embed/OhzlSORyzco');
        break;  
    
    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Sucré</li><li>équilibré</li><li>Vegan</li><li>Africain</li>':
        urlVideos.push('https://www.youtube.com/embed/1e7jVkv2lgY');
        urlVideos.push('https://www.youtube.com/embed/0cVOW1emEzw');
        urlVideos.push('https://www.youtube.com/embed/sYdNarCkSoQ');
        urlVideos.push('https://www.youtube.com/embed/oLQlVXI_CVM');
        urlVideos.push('https://www.youtube.com/embed/tIoerxiv9Yk');
        urlVideos.push('https://www.youtube.com/embed/plT7pLk8vwo');
        break;      

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Sucré</li><li>équilibré</li><li>Omnivore</li><li>Asiatique</li>':
        urlVideos.push('https://www.youtube.com/embed/JWlV5nF8hzE');
        urlVideos.push('https://www.youtube.com/embed/5I3rngz6gQo');
        urlVideos.push('https://www.youtube.com/embed/UdOpSDiBtyM');
        urlVideos.push('https://www.youtube.com/embed/CtSQbAU1NF8');
        urlVideos.push('https://www.youtube.com/embed/zbmb5tZGfdA');
        urlVideos.push('https://www.youtube.com/embed/G1P7PH5vLrI');
        break;
        
    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Sucré</li><li>équilibré</li><li>Omnivore</li><li>Européen</li>':
        urlVideos.push('https://www.youtube.com/embed/qJTVep49EWo');
        urlVideos.push('https://www.youtube.com/embed/lMBhiR5TvNc');
        urlVideos.push('https://www.youtube.com/embed/-0IndrluwvM');
        urlVideos.push('https://www.youtube.com/embed/Wz3MZYWPcvI');
        urlVideos.push('https://www.youtube.com/embed/c7zHYh4B1hY');
        urlVideos.push('https://www.youtube.com/embed/VYEqGg1emPc');
        break;
     
    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Sucré</li><li>équilibré</li><li>Omnivore</li><li>Américain</li>':
        urlVideos.push('https://www.youtube.com/embed/FtqPdIkjhvU');
        urlVideos.push('https://www.youtube.com/embed/Hn_eSwhqj0A');
        urlVideos.push('https://www.youtube.com/embed/fXLYqqxB2wc');
        urlVideos.push('https://www.youtube.com/embed/ZkG1WICdVwU');
        urlVideos.push('https://www.youtube.com/embed/6hOWu0Skfmw');
        urlVideos.push('https://www.youtube.com/embed/OhzlSORyzco');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Sucré</li><li>équilibré</li><li>Omnivore</li><li>Africain</li>':
        urlVideos.push('https://www.youtube.com/embed/1e7jVkv2lgY');
        urlVideos.push('https://www.youtube.com/embed/0cVOW1emEzw');
        urlVideos.push('https://www.youtube.com/embed/sYdNarCkSoQ');
        urlVideos.push('https://www.youtube.com/embed/oLQlVXI_CVM');
        urlVideos.push('https://www.youtube.com/embed/tIoerxiv9Yk');
        urlVideos.push('https://www.youtube.com/embed/plT7pLk8vwo');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Sucré</li><li>Gourmand</li><li>Omnivore</li><li>Asiatique</li>':
        urlVideos.push('https://www.youtube.com/embed/_VKYvJAsBaw');
        urlVideos.push('https://www.youtube.com/embed/IzZa0wByUs4');
        urlVideos.push('https://www.youtube.com/embed/bjHFbeHeUSs');
        urlVideos.push('https://www.youtube.com/embed/UJx2wtu4juM');
        urlVideos.push('https://www.youtube.com/embed/cpx2v7vae8E');
        urlVideos.push('https://www.youtube.com/embed/inikLrl_SOY');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Sucré</li><li>Gourmand</li><li>Omnivore</li><li>Européen</li>':
        urlVideos.push('https://www.youtube.com/embed/2u3SPeKk4FU');
        urlVideos.push('https://www.youtube.com/embed/Pa_YFC9f68I');
        urlVideos.push('https://www.youtube.com/embed/2ANZorxH7Ek');
        urlVideos.push('https://www.youtube.com/embed/Rnm3wwxPdmk');
        urlVideos.push('https://www.youtube.com/embed/u5l3PmOmr6M');
        urlVideos.push('https://www.youtube.com/embed/_VKYvJAsBaw');
        break;
    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Sucré</li><li>Gourmand</li><li>Omnivore</li><li>Européen</li>':
        urlVideos.push('https://www.youtube.com/embed/2u3SPeKk4FU');
        urlVideos.push('https://www.youtube.com/embed/Pa_YFC9f68I');
        urlVideos.push('https://www.youtube.com/embed/2ANZorxH7Ek');
        urlVideos.push('https://www.youtube.com/embed/Rnm3wwxPdmk');
        urlVideos.push('https://www.youtube.com/embed/u5l3PmOmr6M');
        urlVideos.push('https://www.youtube.com/embed/zIHPYQNK0Kw');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Sucré</li><li>Gourmand</li><li>Omnivore</li><li>Américain</li>':
        urlVideos.push('https://www.youtube.com/embed/2v5KqwDQTh4');
        urlVideos.push('https://www.youtube.com/embed/jS9tNJAc_vI');
        urlVideos.push('https://www.youtube.com/embed/Pc3H05rcLN4');
        urlVideos.push('https://www.youtube.com/embed/ke31oqCrC2Q');
        urlVideos.push('https://www.youtube.com/embed/984YcFbhEcs');
        urlVideos.push('https://www.youtube.com/embed/gASg5dKX3Vo');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Sucré</li><li>Gourmand</li><li>Omnivore</li><li>Africain</li>':
        urlVideos.push('https://www.youtube.com/embed/d_IjqCJoLRE');
        urlVideos.push('https://www.youtube.com/embed/Us88SsTGXW8');
        urlVideos.push('https://www.youtube.com/embed/uQf1D7RkS50');
        urlVideos.push('https://www.youtube.com/embed/L7d5uBRWayg');
        urlVideos.push('https://www.youtube.com/embed/DG96Uoqj90U');
        urlVideos.push('https://www.youtube.com/embed/YnxOiH1fW_U');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Sucré</li><li>Gourmand</li><li>Vegan</li><li>Asiatique</li>':
        urlVideos.push('https://www.youtube.com/embed/m5UuLaUQSRw');
        urlVideos.push('https://www.youtube.com/embed/y6Od39XM0fY');
        urlVideos.push('https://www.youtube.com/embed/oPku8HakJwU');
        urlVideos.push('https://www.youtube.com/embed/Q3bZlFovgO4');
        urlVideos.push('https://www.youtube.com/embed/BVxnreSSmx4');
        urlVideos.push('https://www.youtube.com/embed/6zzRlRFalAk');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Sucré</li><li>Gourmand</li><li>Vegan</li><li>Européen</li>':
        urlVideos.push('https://www.youtube.com/embed/YpRmZWB4_KQ');
        urlVideos.push('https://www.youtube.com/embed/EaStrywOa3k');
        urlVideos.push('https://www.youtube.com/embed/B39yqTOi7uU');
        urlVideos.push('https://www.youtube.com/embed/2AFGGquQRmA');
        urlVideos.push('https://www.youtube.com/embed/6Ac3X_jpZ5E');
        urlVideos.push('https://www.youtube.com/embed/A0KM6XVcmio');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Sucré</li><li>Gourmand</li><li>Vegan</li><li>Américain</li>':
        urlVideos.push('https://www.youtube.com/embed/iH8iTR2fIQQ');
        urlVideos.push('https://www.youtube.com/embed/TqQaDWo9cw4');
        urlVideos.push('https://www.youtube.com/embed/RQITSaXAmE8');
        urlVideos.push('https://www.youtube.com/embed/gkT5PRHDHYc');
        urlVideos.push('https://www.youtube.com/embed/D7fXdsgpPAA');
        urlVideos.push('https://www.youtube.com/embed/2zzBsHUr7jE');
        break;

    case '<li>Start</li><li>Faire</li><li>Cuisine</li><li>Rapide</li><li>Sucré</li><li>Gourmand</li><li>Vegan</li><li>Africain</li>':
        urlVideos.push('https://www.youtube.com/embed/QMDaxjc11xc');
        urlVideos.push('https://www.youtube.com/embed/KFtzALdLgfE');
        urlVideos.push('https://www.youtube.com/embed/A9xQQCcfSUE');
        urlVideos.push('https://www.youtube.com/embed/4HLQ6Dai5Rw');
        urlVideos.push('https://www.youtube.com/embed/uDsqocekdcg');
        urlVideos.push('https://www.youtube.com/embed/qyy_7eXtGhs');
        break;
    default:
        // Aucun des case ci-dessus
        console.log('cas non pris en charge')
        break;
  }

  // Affiche le code html des videos dans la zone d'id #resultat
  var htmlVideos = "";

  urlVideos.forEach( urlVideoAct => {

    htmlVideos += `<iframe width="350" height="225" 
    src="${urlVideoAct}" 
    frameborder="0" allow="accelerometer; autoplay; 
    encrypted-media; gyroscope; picture-in-picture" 
    allowfullscreen></iframe>
    `;
  } );

  document.querySelector('#resultat').innerHTML = htmlVideos;
}

//--------------------------------------------------------------------AFFICHAGE DE LA QUESTION

function afficheQuestion(questionId){
  // Récup question à partir de son ID
  question = rechercheQuestionParId(questionId);

  // Complete le texteCheminements
  texteCheminement += '<li>' +question.text + '</li>';
  questionHTML = ``;

  // Recupère les sous-questions
  if( question.idsSousQuestions.length==0 ){
      // Fin du questionnaire
      console.log(texteCheminement);

      afficheResultatQuestionnaire();

  }else{
      // Pas fin du questionnaire
      question.idsSousQuestions.forEach(id => {
            var sousQuestionAct = rechercheQuestionParId(id);

            // Génère html pour les sous-questions
            questionHTML +=`<div class="grid__item ${sousQuestionAct.responsiveItem}" style="background-color:${sousQuestionAct.BgCol};">
                                  <img src="./assets/button-png.png" alt="button" class="button" 
                                    onclick='afficheQuestion(${sousQuestionAct.id})'>
                                  <img src="${sousQuestionAct.IconUrl}" alt="icon" class="icon">
                                  <h2 class="text-answer">${sousQuestionAct.text}</h2>
                            </div>`
            console.log(sousQuestionAct.id);
      });
  }

  // Affichage du cheminement
  document.querySelector('#chemin').innerHTML = texteCheminement;
  document.querySelector('#chemin-mobile').innerHTML = texteCheminement;

  // Remplacer la zone #question par le HTML généré
  document.querySelector('#proposition').innerHTML = questionHTML;
}
  
/**
* Script principal – lancement du questionnaire
*/
var questionAct = questionnaire.questions[0];
var texteCheminement = "";

afficheQuestion('start');

//va chercher les donnees du fichier data.json
//fetch('/data.json').then(rep => {
  //  rep.json().then(object => {
    //    questions = object;

      //  afficheQuestion('start');
    //});
//});
