
function afficheGroupeVideos(categorie, menuLink){

  // Faire apparaître le block scrollableDiv + disparaitre footer
  document.querySelector('.scrollableDiv').style['width']='100%';
  document.querySelector('footer').style['display']='none';
  document.querySelector('.play-button').style['display']='none';


  masquerMenuLink();
  document.querySelector(menuLink).style['color'] = 'white';

  if(categorie == 'sante') {
    document.querySelector('.archives-grid').style['background-color'] = '#88DB8F';
    document.querySelector('.scrollableDiv').style['background-color'] = '#BFE6C2';

  }else if (categorie == 'sport') {
    document.querySelector('.archives-grid').style['background-color'] = '#88DBCD';
    document.querySelector('.scrollableDiv').style['background-color'] = '#B2DDD6';
  
  }else if(categorie == 'histoire') {
    document.querySelector('.archives-grid').style['background-color'] = '#77BA7E';
    document.querySelector('.scrollableDiv').style['background-color'] = '#A9D8AD';
  //cuisine
  } else {
    document.querySelector('.archives-grid').style['background-color'] = '#68A89E';
    document.querySelector('.scrollableDiv').style['background-color'] = '#9BC4BE';
  }

  // Récupère les datas de la catégorie cliquée à partir du tabl videosParCategorie)
  var videoCatTrouvee = videosParCategorie.find( videoCatData => {
    if( videoCatData.categorie==categorie ){
      return videoCatData;
    }else{
      return undefined;
    }
  } );

  // Transforme les donnees de chaque video vers un tableau de string contenant le HTML de chaque video
  var tabVideosHtml = videoCatTrouvee.videos.map( videoDataAct => {
    return genereHtmlVideo(videoDataAct);
  } );
  
  // Joint dans une seule var le contenu (html) de chaque élément html du tabVideosHtml
  var html = tabVideosHtml.join(' ');

  // Affiche les videos dans la zone de droite
  document.querySelector('#Videos').innerHTML = html;
}

function masquerMenuLink() {
  document.querySelectorAll('#sportLink,#santeLink,#histoireLink,#cuisLink').forEach( li =>{
    li.style['color']='';
  } );
} masquerMenuLink();


/** Retourne le code HTML d'une video à partir du param videoData : {url, titre, tags} */
function genereHtmlVideo(videoData){
  var html=`<div class="grid__item">
        <iframe width="150" height="100" src="${videoData.url}" frameborder="0" allowfullscreen></iframe>
        <p class="titre">${videoData.titre}</p>
        <p class="cheminement">${videoData.tags}</p>
      </div>`;

  return html;
}


var videosParCategorie = [
  {
    categorie: 'cuisine',
    videos: [
      {
        url: 'https://www.youtube.com/embed/H4bAVqruiUo',
        titre: 'Feuilles De Vigne Farcies Libanais',
        tags: '#libanais #vegan #équilibré',
      },
      {
        url: 'https://www.youtube.com/embed/oJHNrC_p8hQ',
        titre: 'Paella Vegan',
        tags: '#paella #vegan #équilibré'
      },
      {
        url: 'https://www.youtube.com/embed/MIihsRc8Bi8',
        titre: 'Comment faire les meilleurs Tamales!',
        tags: '#tamales #tamales #vegan'
      },
      {
        url: 'https://www.youtube.com/embed/Rnm3wwxPdmk',
        titre: 'Churros',
        tags: '#rapide #gourmand #sucré #churros #espagne'
      },
      {
        url: 'https://www.youtube.com/embed/IBuPm_urzKo',
        titre: 'Cake Pomme Chaï Sain',
        tags: '#omnivore #équilibré #cake #sain #américain #pomme #chaï'
      },
      {
        url: 'https://www.youtube.com/embed/fmJ9bV9XvUY',
        titre: 'Thiakry',
        tags: '#omnivore #équilibré #sain #africain #Mahalabiya'
      },
      {
        url: 'https://www.youtube.com/embed/sYdNarCkSoQ',
        titre: 'Pad Thaï Végétarien',
        tags: '#padthai #veggie #équilibré'
      },
      {
        url: 'https://www.youtube.com/embed/2Mkjhsw45Ws',
        titre: 'Gâteau coco manioc',
        tags: '#omnivore #équilibré #gâteau #manioc #asiatique'
      },
      {
        url: 'https://www.youtube.com/embed/oYIIoxiDjuk',
        titre: 'Risotto Aux Légumes',
        tags: '#risotto #veggie #équilibré'
      },
      {
        url: 'https://www.youtube.com/embed/qJTVep49EWo',
        titre: 'Tarte au citron saine',
        tags: '#omnivore #équilibré #citron #sain #europe #tarte'
      },
      {
        url: 'https://www.youtube.com/embed/lgZQA8cIiUY',
        titre: 'Gratin aux brocolis et fromage',
        tags: '#gratin #veggie #équilibré'
      },
      {
        url: 'https://www.youtube.com/embed/Vn0NL01RNWc',
        titre: '6 Recettes Chinoises végétariennes',
        tags: '#chinois #veggie #gourmand'
      },
      {
        url: 'https://www.youtube.com/embed/kSb62MGJSI4',
        titre: 'Pizza végétarienne',
        tags: '#pizza #veggie #gourmand'
      },
      {
        url: 'https://www.youtube.com/embed/VdbJqMd0CX0',
        titre: 'Tarte aux tomates',
        tags: '#tarte #tomate #gourmand'
      },
      {
        url: 'https://www.youtube.com/embed/hoh2x3EAMuE',
        titre: 'Hot-dog Traditionnel',
        tags: '#hotdog #tradi #gourmand'
      },
      {
        url: 'https://www.youtube.com/embed/aUo-OWMej3M',
        titre: 'Burger Falafels',
        tags: '#burger #falafels #gourmand'
      },
      {
        url: 'https://www.youtube.com/embed/37w3QumGGss',
        titre: 'Mafé végétarien',
        tags: '#mafé #vege #gourmand'
      },
      {
        url: 'https://www.youtube.com/embed/5k9PHnu1YrE',
        titre: 'Riz aux gombos',
        tags: '#riz #gombos #gourmand'
      },
      {
        url: 'https://www.youtube.com/embed/Im1bcoZfVHQ',
        titre: 'Chawarma Sénégalais',
        tags: '#chawarma #viande #gourmand'
      },
      {
        url: 'https://www.youtube.com/embed/KQlbPyevgUI',
        titre: 'Top 10 recettes Japonaises Vegan',
        tags: '#japon #vegan #gourmand'
      },
      {
        url: 'https://www.youtube.com/embed/8DXUUksyJTw',
        titre: 'Channa Masala Indien',
        tags: '#masala #vegan #gourmand'
      },
      {
        url: 'https://www.youtube.com/embed/aFRGqc5b_EM',
        titre: '5 Recettes vegan',
        tags: '#vegan #europe #gourmand'
      },
      {
        url: 'https://www.youtube.com/embed/8CSb4x3MWhc',
        titre: 'Tortilla Espagnol vegan',
        tags: '#tortilla #vegan #gourmand'
      },
      {
        url: 'https://www.youtube.com/embed/FhJ6QMUKIRc',
        titre: 'Feijoada Brésilienne',
        tags: '#feijoada #vegan #gourmand'
      },
      {
        url: 'https://www.youtube.com/embed/meQiq_WX9wE',
        titre: 'Recettes Américaine vegan',
        tags: '#amercia #vegan #gourmand'
      },
      {
        url: 'https://www.youtube.com/embed/IA9MZMXde14',
        titre: 'SPECIAL PAQUES - Tajine de Seitan / Vegan',
        tags: '#tajine #vegan #gourmand'
      },
      {
        url: 'https://www.youtube.com/embed/UCOe6lAKJ58',
        titre: '5 puddings de chia / vegan',
        tags: '#shia #vegan #équilibré'
      },
      {
        url: 'https://www.youtube.com/embed/SS_B77XgV4I',
        titre: 'Matcha Pudding (raw vegan)',
        tags: '#matcha #pudding #vegan'
      },
      {
        url: 'https://www.youtube.com/embed/YNXXCoIEXZw',
        titre: 'Panna Cotta vegan avec une compote de myrtilles',
        tags: '#pannacotta #vegan #équilibré'
      },
      {
        url: 'https://www.youtube.com/embed/oPMvr6q8wXU',
        titre: 'VEGAN BIRCHER MUESLI',
        tags: '#bircher #vegan #équilibré'
      },
      {
        url: 'https://www.youtube.com/embed/dAJxUggNRts',
        titre: 'Vegan Matcha Green Tea Cheesecake',
        tags: '#matcha #cheesecake #vegan #équilibré'
      },
      {
        url: 'https://www.youtube.com/embed/KgMyC5fzwjE',
        titre: 'Poudine Manioc (Mauritian Cassava Cake)',
        tags: '#Manioc #cake #vegan'
      },
      {
        url: 'https://www.youtube.com/embed/xU2OoJC2US0',
        titre: 'Ngalakh - Dessert Sénégalais',
        tags: '#sénégal #dessert #vegan'
      },
      {
        url: 'https://www.youtube.com/embed/OFHnGNZgCOc',
        titre: 'MATCHA LAVA CAKE RECIPE',
        tags: '#matcha #cake #vegan'
      },
      {
        url: 'https://www.youtube.com/embed/EeXHuYG01wU',
        titre: 'VEGAN MOCHI ICE CREAM!',
        tags: '#mochi #glacé #vegan'
      },
      {
        url: 'https://www.youtube.com/embed/GV1T7TMZK6c',
        titre: 'Dorayaki vegan - recette',
        tags: '#dorayaki #vegan #gourmand'
      },
      {
        url: 'https://www.youtube.com/embed/YpRmZWB4_KQ',
        titre: 'Vegan Tiramisu',
        tags: '#tiramisu #vegan #gourmand'
      },
      {
        url: 'https://www.youtube.com/embed/ocEr9Hw3C_I',
        titre: 'Tarte fraises vegan',
        tags: '#tarte #fraise #vegan'
      },
      {
        url: 'https://www.youtube.com/embed/w9JdSDXD6v8',
        titre: 'Mousse CHOCOLAT VEGAN',
        tags: '#mousse #chocholat #vegan'
      },
      {
        url: 'https://www.youtube.com/embed/nN2l4pcbtv0',
        titre: '6 Must-Try Vegan Desserts',
        tags: '#desserts #vegan #gourmand'
      },
      {
        url: 'https://www.youtube.com/embed/G0aqhIcSBd0',
        titre: 'KEY LIME PIE & MERINGUE',
        tags: '#keylimepie #meringue #vegan'
      },
      {
        url: 'https://www.youtube.com/embed/fsxodKDXm5w',
        titre: 'Vegan Pumpkin Pie',
        tags: '#pumpkin #pie #vegan'
      },
      {
        url: 'https://www.youtube.com/embed/LnI_LTfU9ZI',
        titre: 'Rouleaux de printemps / VEGAN',
        tags: '#rouleaux #printemps #vegan'
      },
      {
        url: 'https://www.youtube.com/embed/IzZa0wByUs4',
        titre: 'Flan libanais',
        tags: '#rapide #gourmand #sucré #flan #libanais'
      },
      {
        url: 'https://www.youtube.com/embed/kBYOmDdFgKE',
        titre: 'Nougats chinois',
        tags: '#rapide #gourmand #sucré #nougat #chinois'
      },
      {
        url: 'https://www.youtube.com/embed/_VKYvJAsBaw',
        titre: 'Un bon goûter facile et rapide',
        tags: '#rapide #gourmand #sucré #goûter'
      },
      {
        url: 'https://www.youtube.com/embed/ke31oqCrC2Q',
        titre: 'Gâteau brésilien',
        tags: '#rapide #gourmand #sucré #gâteau #brésil'
      },
      {
        url: 'https://www.youtube.com/embed/m5UuLaUQSRw',
        titre: 'Perle de coco',
        tags: '#vegan #gourmand #perle #coco #asiatique'
      },
      {
        url: 'https://www.youtube.com/embed/ZDymNGYpl3w',
        titre: 'Sauce de feuille de manioc aux arachides',
        tags: '#rapide #salé #gourmand #vegan #africain'
      },
      {
        url: 'https://www.youtube.com/embed/vHKSzbuNbr0',
        titre: 'Muffins aux myrtilles',
        tags: '#vegan #gourmand #rapide #américain #myrtilles #muffins'
      }
    ]
  },
  {
    categorie: 'sport',
    videos: [
      {
        url: 'https://www.youtube.com/embed/ZtvFNaQcwkc',
        titre: 'BOOTY INTENSE + JAMBES FINES',
        tags: '#basducorps #matériel #muscle #booty'
      },
      {
        url: 'https://www.youtube.com/embed/LuZ9e3EBYOg',
        titre: 'ENTRAÎNEMENT FESSIERS',
        tags: '#basducorps #matériel #muscle'
      },
      {
        url: 'https://www.youtube.com/embed/iqT_G1ci2QU',
        titre: '7 EXERCICES BAS DU CORPS AVEC HALTÈRES',
        tags: '#basducorps #matériel #muscle'
      },
      {
        url: 'https://www.youtube.com/embed/rgxbWGdobOc',
        titre: 'FULL HAUT DU CORPS BICEPS ET DOS (AVEC ELASTICS)',
        tags: '#hautducorps #matériel #abdos #muscle'
      },
      {
        url: 'https://www.youtube.com/embed/va6lh5nZ4R8',
        titre: 'FULL HAUT DU CORPS AVEC HALTÈRES',
        tags: '#hautducorps #matériel #abdos #muscle'
      },
      {
        url: 'https://www.youtube.com/embed/ixKnTL8RFSI',
        titre: 'HAUT DU CORPS COMPLET + ABDOMINAUX INTENSES',
        tags: '#hautducorps #matériel #abdos #muscle'
      },
      {
        url: 'https://www.youtube.com/embed/LwKqBqN8Wyc',
        titre: '15MIN HIIT DEBUTANT HALTÈRES',
        tags: '#pertedegraisse #matériel #hiit #muscle'
      },
      {
        url: 'https://www.youtube.com/embed/tf1tdCYp6X8',
        titre: '35MIN BRÛLE GRAISSE ET CIRCUIT TRAINING MUSCLE',
        tags: '#pertedegraisse #matériel #hiit #muscle'
      },
      {
        url: 'https://www.youtube.com/embed/wJWxLp-g4rM',
        titre: 'HIIT BRÛLE GRAISSE FULL BODY',
        tags: '#pertedegraisse #matériel #hiit'
      },
      {
        url: 'https://www.youtube.com/embed/jE57-u2wRC8',
        titre: 'CARDIO DEBUTANT + BAS DU CORPS',
        tags: '#pertedegraisse #sansmatériel #hiit'
      },
      {
        url: 'https://www.youtube.com/embed/6BUTI9yCz-8',
        titre: 'PERDRE LE GRAS DU VENTRE EN 12MIN!',
        tags: '#pertedegraisse #sansmatériel #hiit'
      },
      {
        url: 'https://www.youtube.com/embed/HjWxrCOyMAs',
        titre: '15MIN CARDIO BRÛLE-GRAISSE INTENSIF',
        tags: '#pertedegraisse #sansmatériel #hiit'
      },
      {
        url: 'https://www.youtube.com/embed/lHILbkfZ9e4',
        titre: '20MIN FULL BODY HIIT',
        tags: '#pertedegraisse #sansmatériel #hiit'
      },
      {
        url: 'https://www.youtube.com/embed/k20aYCK1Ru4',
        titre: '25MIN BAS DU CORPS KETTLEBELL',
        tags: '#muscle #matériel #basducorps'
      },
      {
        url: 'https://www.youtube.com/embed/O6zJdV4nZ4U',
        titre: '15MIN JAMBES ET FESSIERS INTENSES',
        tags: '#muscle #matériel #basducorps'
      },
      {
        url: 'https://www.youtube.com/embed/xPCDR5b9F70',
        titre: 'WORKOUT JAMBES AVEC HALTÈRES',
        tags: '#muscle #matériel #basducorps'
      },
      {
        url: 'https://www.youtube.com/embed/72gZihBC56c',
        titre: 'HAUT DU CORPS ET ABDOS DESSINÉS',
        tags: '#muscle #matériel #hautducorps'
      },
      {
        url: 'https://www.youtube.com/embed/AzIkSv3mo-Y',
        titre: 'HAUT DU CORPS AVEC ALTERES',
        tags: '#muscle #matériel #hautducorps'
      },
      {
        url: 'https://www.youtube.com/embed/-16pyrNVoaE',
        titre: 'SEANCE FITNESS EPAULES, DOS & PECS',
        tags: '#muscle #matériel #hautducorps'
      },
      {
        url: 'https://www.youtube.com/embed/qk3MXOJ60kE',
        titre: 'BRÛLE-GRAISSE, VENTRE PLAT ET SEC & BUSTE TONIFIÉ',
        tags: '#pertedegraisse #athome #extreme'
      },
      {
        url: 'https://www.youtube.com/embed/WxEiJ4Yy8g4',
        titre: 'KETTLEBELL HIIT',
        tags: '#pertedegraisse #athome #extreme'
      },
      {
        url: 'https://www.youtube.com/embed/6EvdR5lFq48',
        titre: 'BRÛLE-GRAISSE EXTREME FULL-BODY',
        tags: '#pertedegraisse #athome #extreme'
      },
      {
        url: 'https://www.youtube.com/embed/ku3vgejAZ0g',
        titre: 'CIRCUIT PERTE DE GRAISSE 10 MINUTES',
        tags: '#pertedegraisse #athome #sansmatériel'
      },
      {
        url: 'https://www.youtube.com/embed/7DXj_d1FrAQ',
        titre: '21 MINUTES PERTE DE GRAISSE (SANS SAUT ET SANS MATÉRIEL)',
        tags: '#pertedegraisse #athome #sansmatériel'
      },
      {
        url: 'https://www.youtube.com/embed/wQvBZPdXUz4',
        titre: '20 MINUTES PERTE DE GRAISSE',
        tags: '#pertedegraisse #athome #sansmatériel'
      },
      {
        url: 'https://www.youtube.com/embed/ZtvFNaQcwkc',
        titre: 'JAMBES FINES ET BOOTY INTENSE',
        tags: '#intense #muscle #jambes #matériel #booty'
      },
      {
        url: 'https://www.youtube.com/embed/dFnL8-D_QdI',
        titre: 'MUSCLER SES JAMBES À LA MAISON',
        tags: '#intense #muscle #jambes #matériel'
      },
      {
        url: 'https://www.youtube.com/embed/afUGTKsym_Y',
        titre: 'FESSIERS INTENSES ET JAMBES',
        tags: '#intense #fessiers #jambes #matériel'
      },
      {
        url: 'https://www.youtube.com/embed/ixKnTL8RFSI',
        titre: 'HAUT DU CORPS COMPLET ET ABDOS INTENSES',
        tags: '#hautducorps #abs #extreme'
      },
      {
        url: 'https://www.youtube.com/embed/yPiANlQxank',
        titre: 'DOS ET BICEPS INTENSE',
        tags: '#hautducorps #matériel #extreme'
      },
      {
        url: 'https://www.youtube.com/embed/ynDyv6LDRTM',
        titre: 'HAUT DU CORPS A LA MAISON (AVEC ÉQUIPEMENT)',
        tags: '#hautducorps #matériel #extreme'
      },
      {
        url: 'https://www.youtube.com/embed/vVZ--4TTD7k',
        titre: 'PERTE DE GRAISSE EXTREME KETTLEBELL',
        tags: '#pertedegraisse #sansmatériel #extreme #fessiers'
      },
      {
        url: 'https://www.youtube.com/embed/cuHwoCWFLIw',
        titre: 'FULL BODY HIIT AVEC POIDS',
        tags: '#pertedegraisse #sansmatériel #extreme #fessiers'
      },
      {
        url: 'https://www.youtube.com/embed/NDvEySqPSio',
        titre: 'BRÛLE GRAISSE ET BOOTY BURN',
        tags: '#pertedegraisse #sansmatériel #extreme #fessiers'
      },
      {
        url: 'https://www.youtube.com/embed/_Zem0_qsDg0',
        titre: '500 CALORIES EN 45 MINUTES',
        tags: '#pertedegraisse #sansmatériel #extreme #athome'
      },
      {
        url: 'https://www.youtube.com/embed/CKUAeaBWu18',
        titre: 'ENTRAINEMENT CARDIO',
        tags: '#pertedegraisse #sansmatériel #extreme #athome'
      },
      {
        url: 'https://www.youtube.com/embed/VUpGLzYJJNM',
        titre: 'CHALLENGE 1000 CALORIES',
        tags: '#pertedegraisse #sansmatériel #extreme #athome'
      },
      {
        url: 'https://www.youtube.com/embed/H4hlvb3MCyw',
        titre: 'ENTRAÎNEMENT FESSIERS A LA MAISON',
        tags: '#basducorps #sansmatériel #jambes #athome #debutant'
      },
      {
        url: 'https://www.youtube.com/embed/jE57-u2wRC8',
        titre: 'FESSIERS ET INTERIEUR DES CUISSES DEBUTANT SANS ÉQUIPEMENT',
        tags: '#basducorps #sansmatériel #jambes #athome #debutant'
      },
      {
        url: 'https://www.youtube.com/embed/htL4XYk20Mc',
        titre: '30MIN JAMBES ET FESSES DÉBUTANT',
        tags: '#basducorps #sansmatériel #jambes #athome #debutant'
      },
      {
        url: 'https://www.youtube.com/embed/BMLhJvhWZ0E',
        titre: '15MIN HAUT DU CORPS DEBUTANT SANS ÉQUIPEMENT',
        tags: '#hautducorps #sansmatériel #athome'
      },
      {
        url: 'https://www.youtube.com/embed/avyAVV2Kd8k',
        titre: 'HAUT DU CORPS DEBUTANT SANS ÉQUIPEMENT',
        tags: '#hautducorps #sansmatériel #athome'
      },
      {
        url: 'https://www.youtube.com/embed/VUTn5-tV4Ak',
        titre: '15MIN HAUT DU CORPS SANS ÉQUIPEMENT',
        tags: '#hautducorps #sansmatériel #athome'
      },
      {
        url: 'https://www.youtube.com/embed/MjPVPDBJ94A',
        titre: '30MIN BAS DU CORPS',
        tags: '#basducorps #sansmatériel #jambes #athome'
      },
      {
        url: 'https://www.youtube.com/embed/9hQTvrP6EsM',
        titre: 'JAMBES TONIFIÉES SANS ÉQUIPEMENT',
        tags: '#basducorps #sansmatériel #jambes #tonifié'
      },
      {
        url: 'https://www.youtube.com/embed/P5egxkKq5ew',
        titre: '10MIN BAS DU CORPS SANS ÉQUIPEMENT',
        tags: '#basducorps #sansmatériel #sport #intense'
      },
      {
        url: 'https://www.youtube.com/embed/70CPGAn9STk',
        titre: '15 MIN HAUT DU CORPS SANS MATÉRIEL',
        tags: '#hautducorps #sansmatériel #sport #intense'
      },
      {
        url: 'https://www.youtube.com/embed/c2KPDUYuBtM',
        titre: '10MIN HAUT DU CORPS SANS MATÉRIEL',
        tags: '#hautducorps #sansmatériel #sport'
      },
      {
        url: 'https://www.youtube.com/embed/aI9VbuedMLI',
        titre: 'ENTRAINEMENT HAUT DU CORPS SANS MATÉRIEL',
        tags: '#hautducorps #sansmatériel #athome'
      },
      {
        url: 'https://www.youtube.com/embed/NDvEySqPSio',
        titre: 'TONIFICATION DES JAMBES ET BOOTY BURN!',
        tags: '#basducorps #jambes #sansmatériel #booty #burn'
      },
      {
        url: 'https://www.youtube.com/embed/_j91KMBtYas',
        titre: 'WORKOUT BAS DU CORPS À LA MAISON',
        tags: '#basducorps #jambes #sansmatériel #athome'
      },
      {
        url: 'https://www.youtube.com/embed/hSgJd--UbKg',
        titre: 'JAMBES FINES ET FESSIERS INTENSIFS',
        tags: '#basducorps #jambes #sansmatériel #fessiers'
      },
      {
        url: 'https://www.youtube.com/embed/A61dLcsS9Ck',
        titre: 'PECTORAUX INTENSES À LA MAISON',
        tags: '#hautducorps #avancé #sansmatériel #intense #pectoraux'
      },
      {
        url: 'https://www.youtube.com/embed/uoqC6Voo8TU',
        titre: 'HAUT DU CORPS EXTRÊME SANS MATERIEL',
        tags: '#hautducorps #avancé #sansmatériel #extreme'
      },
      {
        url: 'https://www.youtube.com/embed/wRL_8yWrkvI',
        titre: 'GAINAGE INTENSE 10 MINUTES',
        tags: '#gainage #hautducorps #avancé #sansmatériel'
      },
      {
        url: 'https://www.youtube.com/embed/Grr-c-ko01g',
        titre: 'CHALLENGE EXTRÊME : 1000 POMPES EN 1 HEURE',
        tags: '#1000 #pompes #hautducorps #avancé #sansmatériel'
      },

    ]
  },
  {
    categorie: 'histoire',
    videos: [
      {
        url: '"https://www.youtube.com/embed/BCfz0K36j9g',
        titre: 'IL ÉTAIT UNE FOIS NOTRE PLANÈTE - VOYAGE AUX ORIGINES DE LA TERRE',
        tags: '#Histoire #Époque #Préhistoire'
      },
      {
        url: 'https://www.youtube.com/embed/bX5zXfrZ3zY',
        titre: 'DOCUMENTAIRE REPORTAGE ARTE EUROPE DEPUIS LA PRÉHISTOIRE',
        tags: '#Histoire #Époque #Préhistoire'
      },
      {
        url: 'https://www.youtube.com/embed/SL2sP8tHgKU',
        titre: 'L\'ODDYSÉE DE L\'ESPACE.AVI ',
        tags: '#Histoire #Époque #Préhistoire'
      },
      {
        url: 'https://www.youtube.com/embed/NdTXF94t7J8',
        titre: 'L\'ÉPOPÉE DE LA GRÈCE ANTIQUE',
        tags: '#histoire #époques #antiquité'
      },
      {
        url: 'https://www.youtube.com/embed/2djs68Vh13U',
        titre: 'LES DERNIERS SECRETS D\'EGYPTE SUR LA PISTE DU TOMBEAU DE NEFERTITI ',
        tags: '#histoire #époques #antiquité'
      },
      {
        url: 'https://www.youtube.com/embed/-mLpXnL9dEc',
        titre: 'LA CHINE ANTIQUE: LE PREMIER EMPEREUR',
        tags: '#histoire #époques #antiquité'
      },
      {
        url: 'https://www.youtube.com/embed/QXfr-5s3exc',
        titre: 'LA VIE DES FRANCAIS AU MOYEN-ÂGE (XIIe - XIIIe SIÈCLE)',
        tags: '#histoire #époques #moyenage'
      },
      {
        url: 'https://www.youtube.com/embed/356sUghcSZg',
        titre: 'DOCUMENTAIRE 1459 LE MANUSCRIT SECRET DU MOYEN-ÂGE',
        tags: '#histoire #époques #moyenage'
      },
      {
        url: 'https://www.youtube.com/embed/356sUghcSZg',
        titre: 'LES ROIS DE FRANCE - CLOVIS, PREMIER ROI DES FRANCS',
        tags: '#histoire #époques #moyenage'
      },
      {
        url: 'https://www.youtube.com/embed/6vTTo47xfds',
        titre: 'MARTIN LUTHER - KARAMBOLAGE - ARTE',
        tags: '#histoire #époques #tempsmodernes'
      },
      {
        url: 'https://www.youtube.com/embed/Gd3m7DxcysY',
        titre: 'HENRI VIII - COMPLOTS À LA COUR',
        tags: '#histoire #époques #tempsmodernes'
      },
      {
        url: 'https://www.youtube.com/embed/C6fRa8vbXh4',
        titre: 'GUTENBERG ET L\'IMPRIMERIE - "RESSOURCES DES ÉCOLES" COLLÈGE',
        tags: '#histoire #époques #tempsmodernes'
      },
      {
        url: 'https://www.youtube.com/embed/VuYftrvJN9o',
        titre: 'DÉCOLONISATION: L\'APPRENTISSAGE - 1857 À 1926',
        tags: '#histoire #époques #époquecontemporaine'
      },
      {
        url: 'https://www.youtube.com/embed/41u5XEpr4q4',
        titre: 'LE PARI FOU DES FRÈRES LUMIÈRE - REPORTAGE - VISITES PRIVÉES',
        tags: '#histoire #époques #époquecontemporaine'
      },
      {
        url: 'https://www.youtube.com/embed/Jf5b42Llujc',
        titre: 'L\'INVENTION DU TÉLÉPHONE',
        tags: '#histoire #époques #époquecontemporaine'
      },
      {
        url: 'https://www.youtube.com/embed/N01db5Y-3B8',
        titre: 'LA GUERRE DE SEPT ANS, LE 1ER CONFLIT MONDIAL !',
        tags: ' #histoire #grandsévénements #guerres #septans'
      },
      {
        url: 'https://www.youtube.com/embed/GXP_K03vWFM',
        titre: 'LA GUERRE DE SEPT ANS - RÉSUMÉ SUR CARTE',
        tags: ' #histoire #grandsévénements #guerres #septans'
      },
      {
        url: 'https://www.youtube.com/embed/UB1CpigwnGs',
        titre: 'PREMIÈRE GUERRE MONDIALE - LE DEBUT 1914 - DOCUMENTAIRE',
        tags: '#histoire #grandsévénements #guerres #premièreguerre'
      },
      {
        url: 'https://www.youtube.com/embed/5B0pE1eCxGU',
        titre: 'LA PREMIÈRE GUERRE MONDIALE - RÉSUMÉ DES GRANDES ÉTAPES',
        tags: '#histoire #grandsévénements #guerres #premièreguerre'
      },
      {
        url: 'https://www.youtube.com/embed/nsB7mM7-o3A',
        titre: 'RÉCIT DE GUERRE: UN MÉDECIN DANS LES TRANCHÉES DURANT LA PREMIÈRE GUERRE MONDIALE',
        tags: '#histoire #grandsévénements #guerres #premièreguerre'
      },
      {
        url: 'https://www.youtube.com/embed/aYFNuwShrhM',
        titre: 'SECONDE GUERRE MONDIALE - RÉSUMÉ DU CONFLIT LE PLUS MEURTRIER DE L\'HISTOIRE',
        tags: ' #histoire #grandsévénements #guerres #deuxièmeguerre'
      },
      {
        url: 'https://www.youtube.com/embed/n_bgNMbnQ2o',
        titre: 'HITLER, POURQUOI SA DÉFAITE ÉTAIT ANNONCÉE',
        tags: ' #histoire #grandsévénements #guerres #deuxièmeguerre'
      },
      {
        url: 'https://www.youtube.com/embed/SRweS3czci8',
        titre: 'TOUTE L\'HISTOIRE DE LA SECONDE GUERRE MONDIALE',
        tags: ' #histoire #grandsévénements #guerres #deuxièmeguerre'
      },
      {
        url: 'https://www.youtube.com/embed/OdWs1EL1WN4',
        titre: 'LA GUERRE FROIDE - HISTOIRE - BREVET',
        tags: '#histoire #grandsévénements #guerres #guerrefroide'
      },
      {
        url: 'https://www.youtube.com/embed/MhfraAqwZD0',
        titre: 'LA GUERRE FROIDE - RÉSUMÉ DES GRANDES ÉTAPES DU CONFLIT',
        tags: '#histoire #grandsévénements #guerres #guerrefroide'
      },
      {
        url: 'https://www.youtube.com/embed/vYuj97AlsFo',
        titre: 'LA GUERRE FROIDE (1970-82)',
        tags: '#histoire #grandsévénements #guerres #guerrefroide'
      },
      {
        url: 'https://www.youtube.com/embed/T--268qZCY8',
        titre: 'CHRISTOPHE COLOMB ET LE NOUVEAU MONDE',
        tags: ' #histoire #grandsévénements #découvertes #monde'
      },
      {
        url: 'https://www.youtube.com/embed/J_4RlrGerEg',
        titre: '5° - LES GRANDES DÉCOUVERTES',
        tags: '#histoire #grandsévénements #découvertes #monde'
      },
      {
        url: 'https://www.youtube.com/embed/c3zFYUlYR9k',
        titre: 'QUI ÉTAIT VRAIMENT MARCO POLO ?',
        tags: '#histoire #grandsévénements #découvertes #monde'
      },
      {
        url: 'https://www.youtube.com/embed/rKF1PLguCjM',
        titre: 'DOCUMENTAIRE REPORTAGE PARAMORMAL THOMAS EDISON ET LE ROYAUME DE L\'AU DELÀ 2019',
        tags: '#histoire #grandsévénements #découvertes #inventions #ThomasEdison'
      },
      {
        url: 'https://www.youtube.com/embed/zmlFAmjQGjQ',
        titre: 'LE PORTRAIT: EP. 7 - THOMAS EDISON',
        tags: '#histoire #grandsévénements #découvertes #inventions #ThomasEdison'
      },
      {
        url: 'https://www.youtube.com/embed/XvYJKfcbrWA',
        titre: 'TOP 10 INVENTION BY THOMAS EDISON',
        tags: '#histoire #grandsévénements #découvertes #inventions #ThomasEdison'
      },
      {
        url: 'https://www.youtube.com/embed/-Vaylhlhv3U',
        titre: 'LE PORTRAIT: NIKOLA TESLA, UNE VIE DE LABEUR',
        tags: ' #histoire #grandsévénements #découvertes #inventions #NikolaTesla'
      },
      {
        url: 'https://www.youtube.com/embed/nKFXoqVMoSQ',
        titre: 'NIKOLA TESLA, LE GENIE OUBLIÉ',
        tags: ' #histoire #grandsévénements #découvertes #inventions #NikolaTesla'
      },
      {
        url: 'https://www.youtube.com/embed/mxpmu4pdj8Q',
        titre: 'NILOLA TESLA, UN GENIE PAS COMME LES AUTRES',
        tags: ' #histoire #grandsévénements #découvertes #inventions #NikolaTesla'
      },
      {
        url: 'https://www.youtube.com/YVYSFl4M5B8',
        titre: 'ALBERT EINSTEIN : L’HOMME ET LE GÉNIE',
        tags: ' #histoire #grandsévénements #découvertes #inventions #AlbertEinstein'
      },
      {
        url: 'https://www.youtube.com/Ll3mDQx07Yg',
        titre: 'ALBERT EINSTEIN , LE GÉNIE DU 20E SIÈCLE',
        tags: ' #histoire #grandsévénements #découvertes #inventions #AlbertEinstein'
      },
      {
        url: 'https://www.youtube.com/M02c1zp0f0w',
        titre: 'LA THÉORIE DE LA RELATIVITÉ EXPLIQUÉE PAR L\'OBSERVATOIRE DE PARIS',
        tags: ' #histoire #grandsévénements #découvertes #inventions #AlbertEinstein'
      },
      {
        url: '"https://www.youtube.com/embed/xACFN-GiDaA',
        titre: 'TU CONNAIS ISAAC NEWTON ?',
        tags: ' #histoire #grandsévénements #découvertes #inventions #IsaacNewton'
      },
      {
        url: 'https://www.youtube.com/embed/0cpV4T8r6rc',
        titre: 'ISAAC NEWTON - DOSSIER SECRETS - DOCUMENTAIRE SCIENCE/HISTOIRE',
        tags: ' #histoire #grandsévénements #découvertes #inventions #IsaacNewton'
      },
      {
        url: 'https://www.youtube.com/embed/224e9Ykwta8',
        titre: 'INTERVIEW D\'ISAAC NEWTON (PAR AMANDINE AFTALION)',
        tags: ' #histoire #grandsévénements #découvertes #inventions #IsaacNewton'
      },
      {
        url: 'https://www.youtube.com/jlnc4vz-vDg',
        titre: 'MARIE CURIE - LES GRANDS DÉCOUVREURS - QUELLE HISTOIRE',
        tags: '#histoire #grandsévénements #découvertes #inventions #MarieCurie'
      },
      {
        url: 'https://www.youtube.com/lKfhVLqadDQ',
        titre: 'UNE VIE : MARIE CURIE',
        tags: '#histoire #grandsévénements #découvertes #inventions #MarieCurie'
      },
      {
        url: 'https://www.youtube.com/Y0ZqYwf1aj4',
        titre: 'ORIGINE, EXPANSION ET DÉCLIN DE L\'EMPIRE ROMAIN',
        tags: '#histoire #grandsévènements #chuteempireromain'
      },
      {
        url: 'https://www.youtube.com/TJWL0fQg7as',
        titre: 'ROME, SON EMPIRE : DE LA NAISSANCE À LA CHUTE',
        tags: '#histoire #grandsévènements #chuteempireromain'
      },
      {
        url: 'https://www.youtube.com/_0qhMjTCYOk',
        titre: 'LES ROMAINS - DE LA PRÉHISTOIRE À L\'ANTIQUITÉ',
        tags: '#histoire #grandsévènements #chuteempireromain'
      },
      {
        url: 'https://www.youtube.com/embed/C3THYevbKNE',
        titre: 'LE DROIT DE VOTE DES FEMMES - LA GRANDE EXPLICATION',
        tags: ' #histoire #grandsévénements #femmes'
      },
      {
        url: 'https://www.youtube.com/embed/BfW72FjVC6k',
        titre: '1964: À QUOI SERVENT LES FEMMES ? | ARCHIVE INA',
        tags: ' #histoire #grandsévénements #femmes'
      },
      {
        url: 'https://www.youtube.com/yuBxzyndcDs',
        titre: 'HOMME DANS ESPACE : HISTOIRE D’UNE CONQUÊTE - C\'EST PAS SORCIER',
        tags: '#histoire #grandsévènements #lune'
      },
      {
        url: 'https://www.youtube.com/QGTKpmsNAGw',
        titre: 'APOLLO 11 - LES PREMIERS PAS',
        tags: '#histoire #grandsévènements #lune'
      },
      
    ]
  },
  {
    categorie: 'sante',
    videos: [
      {
        url: 'https://www.youtube.com/embed/NILRFyzSsPg',
        titre: 'DOCUMENTAIRE ARTE ADDICTION',
        tags: '#Santé #Mental #Addiction'
      },
      {
        url: 'https://www.youtube.com/embed/3EB1Kxrsx6A',
        titre: 'POURQUOI INSTAGRAM REND ADDICT ?',
        tags: '#Santé #Mental #Addiction'
      },
      {
        url: 'https://www.youtube.com/embed/DyK4vxbAmwQ',
        titre: 'l\'ADDICTION AUX ECRANS',
        tags: '#Santé #Mental #Addiction'
      },
      {
        url: 'https://www.youtube.com/embed/th4nKyJxmt8',
        titre: 'SORTIR DES ADDICTIONS',
        tags: '#Santé #Mental #Addiction'
      },
      {
        url: 'https://www.youtube.com/embed/6k1J_h4OxQg',
        titre: 'LES YOUTUBEURS ET LEURS PROBLEMES PSYCHOLOGIQUES',
        tags: '#Santé #Mental #Addiction'
      },
      {
        url: 'https://www.youtube.com/embed/KqAo0KHCyts',
        titre: 'LA DEPRESSION, LES NOUVELLES VOIES DE LA GUERISON',
        tags: '#Santé #Mental #Dépression'
      },
      {
        url: 'https://www.youtube.com/embed/J2JvaQHaSt8',
        titre: 'LA DEPRESSION, DANS TON CORPS',
        tags: '#Santé #Mental #Dépression'
      },
      {
        url: 'https://www.youtube.com/embed/KMNXqj3QmxU',
        titre: '6 BULLSHIT SUR LA DEPRESSION',
        tags: '#Santé #Mental #Dépression'
      },
      {
        url: 'https://www.youtube.com/embed/6k1J_h4OxQg',
        titre: 'LES YOUTUBEURS ET LEURS PROBLEMES PSYCHOLOGIQUES',
        tags: '#Santé #Mental #Dépression'
      },
      {
        url: 'https://www.youtube.com/embed/rSW_kt9E3Qs',
        titre: 'TROUBLES DU COMPORTEMENT ALIMENTAIRE EPISODE 1/3',
        tags: '#Santé #Mental #troubleducomportements'
      },
      {
        url: 'https://www.youtube.com/embed/d06EXH6U_Ng',
        titre: 'TROUBLES DU COMPORTEMENT ALIMENTAIRE EPISODE 2/3',
        tags: '#Santé #Mental #troubleducomportements'
      },
      {
        url: 'https://www.youtube.com/embed/DVhcAZM8Oyg',
        titre: 'TROUBLES DU COMPORTEMENT ALIMENTAIRE EPISODE 3/3',
        tags: '#Santé #Mental #troubleducomportements'
      },
      {
        url: 'https://www.youtube.com/embed/IcRLBYhFYhI',
        titre: 'VIVRE AVEC UN TROUBLE DU COMPORTEMENT',
        tags: '#Santé #Mental #troubleducomportements'
      },
      {
        url: 'https://www.youtube.com/embed/QkpUkbFlHy8',
        titre: 'QU\'EST-CE QUE LA SCHIZOPHRENIE ?',
        tags: '#Santé #Mental #Schizophrénie'
      },
      {
        url: 'https://www.youtube.com/embed/rGuwoKTjhBo',
        titre: 'JE VIS AVEC UN SCHIZOPHRENE',
        tags: '#Santé #Mental #schizophrénie'
      },
      {
        url: 'https://www.youtube.com/embed/OGJN-F6BZ8A',
        titre: 'VIVRE AVEC LA SCHIZOPHRENIE',
        tags: '#Santé #Mental #Schizophrénie'
      },
      {
        url: 'https://www.youtube.com/embed/-Dz4cZmU8f8',
        titre: 'SANTE - UN AUTRE REGARD SUR LA SCHIZOPHRENIE',
        tags: '#Santé #Mental #Schizophrénie'
      },
      {
        url: 'https://www.youtube.com/embed/4BHhry5F564',
        titre: 'COMMENT DIAGNOSTIQUER LES TROUBLES BIPOLAIRES ?',
        tags: '#Santé #Mental #Bipolaire'
      },
      {
        url: 'https://www.youtube.com/embed/l8nmYMwd4FU',
        titre: 'BIPOLARITE: A QUOI RESSEMBLE LEUR QUOTIDIEN ?',
        tags: '#Santé #Mental #Bipolaire'
      },
      {
        url: 'https://www.youtube.com/embed/rsfuR-uDeM0',
        titre: 'COMMENT SORTIR DES TROUBLES BIPOLAIRES ?',
        tags: '#Santé #Mental #Bipolaire'
      },
      {
        url: 'https://www.youtube.com/embed/drSiOlmp7N4',
        titre: 'C\'EST PAS SORCIER - NOS POUMONS SOURCE D\'INSPIRATION',
        tags: '#Santé #Physique #système respiratoire'
      },
      {
        url: 'https://www.youtube.com/embed/HoZss4Sc1E0',
        titre: '4 OU 5 CHOSE QUE VOUS IGNOREZ SUR LA RESPIRATION',
        tags: '#Santé #Physique #système respiratoire'
      },
      {
        url: 'https://www.youtube.com/embed/tYTbbPSGIHk',
        titre: 'AU COEUR DES ORGANES: LA RESPIRATION',
        tags: '#Santé #Physique #système respiratoire'
      },
      {
        url: 'https://www.youtube.com/embed/LgHgOxtonmM',
        titre: 'LA NUTRITION - C\'EST PAS SORCIER',
        tags: '#Santé #Physique #Système digestif'
      },
      {
        url: 'https://www.youtube.com/embed/AnmHhWsGQdA',
        titre: 'AU COEUR DES ORGANES: LA DIGESTION',
        tags: '#Santé #Physique #Système digestif'
      },
      {
        url: 'https://www.youtube.com/embed/brEKIsETGfw',
        titre: 'LES SUPER-POUVOIRES DE L\'INTESTIN',
        tags: '#Santé #Physique #Système digestif'
      },
      {
        url: 'https://www.youtube.com/embed/s7SuTXiGupQ',
        titre: 'C\'EST PAS SORCIER - HISTOIRES DE COEURS',
        tags: '#Santé #Physique #système circulatoire'
      },
      {
        url: 'https://www.youtube.com/embed/rBZyKYoh2BU',
        titre: 'AU COEURS DES ORGANES: COEUR ET VAISSEAUX',
        tags: '#Santé #Physique #système circulatoire'
      },
      {
        url: 'https://www.youtube.com/embed/u7qZAs5lhBs',
        titre: 'NOTRE COEUR; UN MUSCLE FRAGILE',
        tags: '#Santé #Physique #système circulatoire'
      },
      {
        url: 'https://www.youtube.com/embed/891NUdFbB04',
        titre: 'LA FABRIQUE DU CERVEAU - SCIENCE ARTE',
        tags: '#Santé #Physique #système nerveux '
      },
      {
        url: 'https://www.youtube.com/embed/hXFBWXuZIdo',
        titre: 'COMMENT CA MARCHE: LE CERVEAU ET LES NEURONNES',
        tags: '#Santé #Physique #système nerveux '
      },
      {
        url: 'https://www.youtube.com/embed/iNSG9O_Iw2w',
        titre: 'LE CERVEAU: UNE ,ACHINE HYPERCONNECTÉE',
        tags: '#Santé #Physique #système nerveux '
      },
      {
        url: 'https://www.youtube.com/embed/IzbcwsHy4v8',
        titre: 'C\'EST PAS SORCIER - SQUELETTE: LES SORCIER TOMBENT SUR UN OS',
        tags: '#Santé #Physique #système musculaire #tissus osseux'
      },
      {
        url: 'https://www.youtube.com/embed/REWwSmnYuM0',
        titre: 'AU COEUR DES ORGANES: LE MUSCLE, MOTEUR DU MOUVEMENT',
        tags: '#Santé #Physique #système musculaire #tissus osseux'
      },
      {
        url: 'https://www.youtube.com/embed/xyQ-UGWQWe8',
        titre: 'DE QUOI SONT COMPOSEE LES OS ?',
        tags: '#Santé #Physique #système musculaire #tissus osseux'
      },
      {
        url: 'https://www.youtube.com/embed/xoayhLS2-eM',
        titre: 'LE CANCER C\'EST QUOI ?',
        tags: '#Santé #Physique #Maladie #Cancer'
      },
      {
        url: 'https://www.youtube.com/embed/5Acsnl1gEzI',
        titre: 'LE CANCER EST-IL UN ORGANISME VIVANT ?',
        tags: '#Santé #Physique #Maladie #Cancer'
      },
      {
        url: 'https://www.youtube.com/embed/YUK131ZKPtk',
        titre: '36.9° - NOUVELLES THERAPIE CONTRE LE CANCER',
        tags: '#Santé #Physique #Maladie #Cancer'
      },
      {
        url: 'https://www.youtube.com/embed/0PNjvwEInYM',
        titre: 'REPENSER ALZHEIMER & CO',
        tags: '#Santé #Physique #Maladie #Alzheimer'
      },
      {
        url: 'https://www.youtube.com/embed/MK2Y3jDZr4I',
        titre: 'ALZHEIMER: COMMENT LUTTER CONTRE LA MALADIE',
        tags: '#Santé #Physique #Maladie #Alzheimer'
      },
      {
        url: 'https://www.youtube.com/embed/tUGsrTirpAg',
        titre: 'ALZHEIMER: LES CAUSES DE LA MALADIE',
        tags: '#Santé #Physique #Maladie #Alzheimer'
      },
      {
        url: 'https://www.youtube.com/embed/FCa3K4d6KL0',
        titre: 'MALADIE DE PARKINSON: DE LA CELLULE A L\'HOMME',
        tags: '#Santé #Physique #Maladie #Parkinson'
      },
      {
        url: 'https://www.youtube.com/embed/EC2IAwxCe-Q',
        titre: 'UN NOUVEL ESPOIR DANS LA MALADIE DE PARKINSON',
        tags: '#Santé #Physique #Maladie #Parkinson'
      },
      {
        url: 'https://www.youtube.com/embed/-XkdsBg7nfc',
        titre: 'LA MALADIE DE PARKINSON',
        tags: '#Santé #Physique #Maladie #Parkinson'
      },
      {
        url: 'https://www.youtube.com/embed/KKJInAkxnLY',
        titre: 'LES ORIGINES DU SIDA',
        tags: '#Santé #Physique #Maladie #Sida'
      },
      {
        url: 'https://www.youtube.com/embed/qWQptSNZemM',
        titre: '5 BULLSHIT SUR LE SIDA',
        tags: '#Santé #Physique #Maladie #Sida'
      },
      {
        url: 'https://www.youtube.com/embed/ujZkefAlV_E',
        titre: 'VIH/SIDA: CE QU\'IL FAUT SAVOIR SUR LA MALADIE AUJOURD\'HUI',
        tags: '#Santé #Physique #Maladie #Sida'
      },
      {
        url: 'https://www.youtube.com/embed/8mo8rWWJZkc',
        titre: 'OMS: LA GRIPPE, UNE MENACE IMPREVISIBLE',
        tags: '#Santé #Physique #Maladie #Grippe'
      },
      {
        url: 'https://www.youtube.com/embed/bM7AOBxqjnE',
        titre: 'CETTE PENDEMIE, VUE DEPUIS 2021',
        tags: '#Santé #Physique #Maladie #Grippe'
      },
      {
        url: 'https://www.youtube.com/embed/R8tFxpRYy6c',
        titre: 'CORONAVIRUS VS GRIPPE: COMMENT LES DIFFERENCIER',
        tags: '#Santé #Physique #Maladie #Grippe'
      }
    ]
  }
];
