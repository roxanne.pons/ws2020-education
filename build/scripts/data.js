//santé: id 4->22
//histoire: id 23->45
//sport: id 46->56
//cuisine: id 57->69

var questionnaire = {
  questions: [   
        {
          id: 'start',
          text: 'Start',
          BgCol: '#6896A8',
          idsSousQuestions: [0,1]
        },
        {
          id: 0,
          text: 'Faire',
          IconUrl: './assets/picto_faire.png',
          BgCol: '#6896A8;',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [46,47]
        },
        {
          id: 1,
          text: 'Apprendre',
          IconUrl: './assets/picto_apprendre.png',
          BgCol: '#88C4DB;',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [2,3]
        },
        {
          id: 2,
          text: 'Santé',
          IconUrl: './assets/picto/sante/picto_sante.png',
          BgCol: '#88DB8F',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [4,5]
        },
        {
          id: 3,
          text: 'Histoire',
          IconUrl: './assets/picto/histoire/picto_histoire.png',
          BgCol: '#68A86E',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [23, 24]
        },
        {
          id: 4,
          text: 'Physique',
          IconUrl: './assets/picto/sante/picto_physique.png',
          BgCol: '#88DB8F',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [6,7]
        },
        {
          id: 5,
          text: 'Mentale',
          IconUrl: './assets/picto/sante/picto_mental.png',
          BgCol: '#88DB8F',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [8,9,10,11,12]
        },
        {
          id: 6,
          text: 'Maladie',
          IconUrl: './assets/picto/sante/picto_maladies.png',
          BgCol: '#88DB8F',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [13,14,15,16,17]
        },
        {
          id: 7,
          text: 'Système',
          IconUrl: './assets/picto/sante/picto_systemes.png',
          BgCol: '#88DB8F',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [18,19,20,21,22]
        },
        {
          id: 8,
          text: 'Addiction',
          IconUrl: './assets/picto/sante/picto_addictions.png',
          BgCol: '#88DB8F',
          idsSousQuestions: []
        },
        {
          id: 9,
          text: 'Dépression',
          IconUrl: './assets/picto/sante/picto_depression.png',
          BgCol: '#88DB8F',
          idsSousQuestions: []
        },
        {
          id: 10,
          text: 'Trouble du comportement',
          IconUrl: './assets/picto/sante/picto_trouble_comportement.png',
          BgCol: '#88DB8F',
          idsSousQuestions: []
        },
        {
          id: 11,
          text: 'Schizophrénie',
          IconUrl: './assets/picto/sante/picto_schizo.png',
          BgCol: '#88DB8F',
          idsSousQuestions: []
        },
        {
          id: 12,
          text: 'Bipolarité',
          IconUrl: './assets/picto/sante/picto_bipolarite.png',
          BgCol: '#88DB8F',
          responsiveItem: 'responsive-item--solo',
          idsSousQuestions: []
        },
        {
          id: 13,
          text: 'Cancer',
          IconUrl: './assets/picto/sante/picto_cancer.png',
          BgCol: '#88DB8F',
          idsSousQuestions: []
        },
        {
          id: 14,
          text: 'Alzheimer',
          IconUrl: './assets/picto/sante/picto_alzheimer.png',
          BgCol: '#88DB8F',
          idsSousQuestions: []
        },
        {
          id: 15,
          text: 'Parkinson',
          IconUrl: './assets/picto/sante/picto_parkinson.png',
          BgCol: '#88DB8F',
          idsSousQuestions: []
        },
        {
          id: 16,
          text: 'Sida',
          IconUrl: './assets/picto/sante/picto_sida.png',
          BgCol: '#88DB8F',
          idsSousQuestions: []
        },
        {
          id: 17,
          text: 'Grippe',
          IconUrl: './assets/picto/sante/picto_grippe.png',
          BgCol: '#88DB8F',
          responsiveItem: 'responsive-item--solo',
          idsSousQuestions: []
        },
        {
          id: 18,
          text: 'Respiratoire',
          IconUrl: './assets/picto/sante/picto_systeme_pulmonaire.png',
          BgCol: '#88DB8F',
          idsSousQuestions: []
        },
        {
          id: 19,
          text: 'Digestif',
          IconUrl: './assets/picto/sante/picto_systeme_digestif.png',
          BgCol: '#88DB8F',
          idsSousQuestions: []
        },
        {
          id: 20,
          text: 'Circulatoire',
          IconUrl: './assets/picto/sante/picto_systeme_circulatoire.png',
          BgCol: '#88DB8F',
          idsSousQuestions: []
        },
        {
          id: 21,
          text: 'Nerveux',
          IconUrl: './assets/picto/sante/picto_systeme_nerveux.png',
          BgCol: '#88DB8F',
          idsSousQuestions: []
        },
        {
          id: 22,
          text: 'Musculaire/Squelettique',
          IconUrl: './assets/picto/sante/picto_systeme_squelettique_01.png',
          BgCol: '#88DB8F',
          responsiveItem: 'responsive-item--solo',
          idsSousQuestions: []
        },//----------------fin de santé / debut histoire
        {
          id: 23,
          text: 'Epoque',
          IconUrl: './assets/picto/histoire/picto_epoques.png',
          BgCol: '#68A86E',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [25,26,27,28,29]
        },
        {
          id: 24,
          text: 'Grands événements',
          IconUrl: './assets/picto/histoire/picto_grands_evenements.png',
          BgCol: '#68A86E',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [30,31,32,33,34]
        },
        {
          id: 25,
          text: 'Préhistoire',
          IconUrl: './assets/picto/histoire/picto_prehistoire.png',
          BgCol: '#68A86E',
          idsSousQuestions: []
        },
        {
          id: 26,
          text: 'Antiquité',
          IconUrl: './assets/picto/histoire/picto_antiquite.png',
          BgCol: '#68A86E',
          idsSousQuestions: []
        },
        {
          id: 27,
          text: 'Moyen-âge',
          IconUrl: './assets/picto/histoire/picto_moyen_age.png',
          BgCol: '#68A86E',
          idsSousQuestions: []
        },
        {
          id: 28,
          text: 'Moderne',
          IconUrl: './assets/picto/histoire/picto_temps_modernes.png',
          BgCol: '#68A86E',
          idsSousQuestions: []
        },
        {
          id: 29,
          text: 'Contemporaine',
          IconUrl: './assets/picto/histoire/picto_epoque_contemporaine.png',
          BgCol: '#68A86E',
          responsiveItem: 'responsive-item--solo',
          idsSousQuestions: []
        },
        {
          id: 30,
          text: 'Guerres',
          IconUrl: './assets/picto/histoire/picto_guerres.png',
          BgCol: '#68A86E',
          idsSousQuestions: [35,36,37,38]
        },
        {
          id: 31,
          text: 'Découvertes',
          IconUrl: './assets/picto/histoire/picto_decouverte.png',
          BgCol: '#68A86E',
          idsSousQuestions: [39,40]
        },
        {
          id: 32,
          text: 'Empire Romain',
          IconUrl: './assets/picto/histoire/picto_chute_empire_romain.png',
          BgCol: '#68A86E',
          idsSousQuestions: []
        },
        {
          id: 33,
          text: 'Femmes',
          IconUrl: './assets/picto/histoire/picto_femmes.png',
          BgCol: '#68A86E',
          idsSousQuestions: []
        },
        {
          id: 34,
          text: 'Lune',
          IconUrl: './assets/picto/histoire/picto_premier_pas_lune.png',
          BgCol: '#68A86E',
          responsiveItem: 'responsive-item--solo',
          idsSousQuestions: []
        },
        {
          id: 35,
          text: 'Sept ans',
          IconUrl: './assets/picto/histoire/picto_guerre_7_ans.png',
          BgCol: '#68A86E',
          idsSousQuestions: []
        },
        {
          id: 36,
          text: 'Première guerre',
          IconUrl: './assets/picto/histoire/picto_1ere_guerre_mondiale.png',
          BgCol: '#68A86E',
          idsSousQuestions: []
        },
        {
          id: 37,
          text: 'Deuxième guerre',
          IconUrl: './assets/picto/histoire/picto_2e_guerre_mondiale.png',
          BgCol: '#68A86E',
          idsSousQuestions: []
        },
        {
          id: 38,
          text: 'Guerre froide',
          IconUrl: './assets/picto/histoire/picto_guerre_froide.png',
          BgCol: '#68A86E',
          idsSousQuestions: []
        },
        {
          id: 39,
          text: 'Monde',
          IconUrl: './assets/picto/histoire/picto_monde.png',
          BgCol: '#68A86E',
          responsiveItem: 'responsive-item',
          idsSousQuestions: []
        },
        {
          id: 40,
          text: 'Inventions',
          IconUrl: './assets/picto/histoire/picto_inventions.png',
          BgCol: '#68A86E',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [41,42,43,44,45]
        },
        {
          id: 41,
          text: 'Thomas Edison',
          IconUrl: './assets/picto/histoire/picto_thomas_edison.png',
          BgCol: '#68A86E',
          idsSousQuestions: []
        },
        {
          id: 42,
          text: 'Nikola Tesla',
          IconUrl: './assets/picto/histoire/picto_Nikola_tesla.png',
          BgCol: '#68A86E',
          idsSousQuestions: []
        },
        {
          id: 43,
          text: 'Albert Einstein',
          IconUrl: './assets/picto/histoire/picto_Albert_Einstein.png',
          BgCol: '#68A86E',
          idsSousQuestions: []
        },
        {
          id: 44,
          text: 'Isaac Newton',
          IconUrl: './assets/picto/histoire/picto_isaac_newton.png',
          BgCol: '#68A86E',
          idsSousQuestions: []
        },
        {
          id: 45,
          text: 'Marie Curie',
          IconUrl: './assets/picto/histoire/picto_MArie_Curie.png',
          BgCol: '#68A86E',
          responsiveItem: 'responsive-item--solo',
          idsSousQuestions: []
        },//-----------------fin histoire/debut sport
        {
          id: 46,
          text: 'Sport',
          IconUrl: './assets/picto/sport/Picto_sport.png',
          BgCol: '#88DBCD',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [48,49,50]
        },
        {
          id: 47,
          text: 'Cuisine',
          IconUrl: './assets/picto/cuisine/picto_cuisine.png',
          BgCol: '#68A89E',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [57,58]
        },
        {
          id: 48,
          text: 'Débutant',
          IconUrl: './assets/picto/sport/picto_debutant.png',
          BgCol: '#88DBCD',
          idsSousQuestions: [51,52]
        },
        {
          id: 49,
          text: 'Intermédiaire',
          IconUrl: './assets/picto/sport/picto_intermediaire.png',
          BgCol: '#88DBCD',
          idsSousQuestions: [51,52]
        },
        {
          id: 50,
          text: 'Avancé',
          IconUrl: './assets/picto/sport/picto_expert.png',
          BgCol: '#88DBCD',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [51,52]
        },
        {
          id: 51,
          text: 'Avec équipement',
          IconUrl: './assets/picto/sport/picto_avec_equipement.png',
          BgCol: '#88DBCD',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [53,54]
        },
        {
          id: 52,
          text: 'Sans équipement',
          IconUrl: './assets/picto/sport/picto_sans_equipement.png',
          BgCol: '#88DBCD',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [53,54]
        },
        {
          id: 53,
          text: 'Musculation',
          IconUrl: './assets/picto/sport/picto_musculation.png',
          BgCol: '#88DBCD',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [55,56]
        },
        {
          id: 54,
          text: 'Perte de graisse',
          IconUrl: './assets/picto/sport/picto_perte_poids.png',
          BgCol: '#88DBCD',
          responsiveItem: 'responsive-item',
          idsSousQuestions: []
        },
        {
          id: 55,
          text: 'Bas du corps',
          IconUrl: './assets/picto/sport/picto_bas_corps.png',
          BgCol: '#88DBCD',
          responsiveItem: 'responsive-item',
          idsSousQuestions: []
        },
        {
          id: 56,
          text: 'Haut du corps',
          IconUrl: './assets/picto/sport/picto_haut_corps.png',
          BgCol: '#88DBCD',
          responsiveItem: 'responsive-item',
          idsSousQuestions: []
        },//--------------fin de sport/debut cuisine
        {
          id: 57,
          text: 'Rapide',
          IconUrl: './assets/picto/cuisine/picto_rapide.png',
          BgCol: '#68A89E',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [59,60]
        },
        {
          id: 58,
          text: 'élaboré',
          IconUrl: './assets/picto/cuisine/picto_elabore.png',
          BgCol: '#68A89E',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [59,60]
        },
        {
          id: 59,
          text: 'Sucré',
          IconUrl: './assets/picto/cuisine/picto_sucre.png',
          BgCol: '#68A89E',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [61,62]
        },
        {
          id: 60,
          text: 'Salé',
          IconUrl: './assets/picto/cuisine/picto_sale.png',
          BgCol: '#68A89E',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [61,62]
        },
        {
          id: 61,
          text: 'Gourmand',
          IconUrl: './assets/picto/cuisine/picto_gourmand_sucre.png',
          BgCol: '#68A89E',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [63,64]
        },
        {
          id: 62,
          text: 'équilibré',
          IconUrl: './assets/picto/cuisine/picto_equilibre.png',
          BgCol: '#68A89E',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [63,64]
        },
        {
          id: 63,
          text: 'Omnivore',
          IconUrl: './assets/picto/cuisine/picto_omnivore.png',
          BgCol: '#68A89E',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [66,67,68,69]
        },
        {
          id: 64,
          text: 'Vegan',
          IconUrl: './assets/picto/cuisine/picto_vegan.png',
          BgCol: '#68A89E',
          responsiveItem: 'responsive-item',
          idsSousQuestions: [66,67,68,69]
        },
        {
          id: 66,
          text: 'Asiatique',
          IconUrl: './assets/picto/cuisine/picto_asiatique.png',
          BgCol: '#68A89E',
          idsSousQuestions: []
        },
        {
          id: 67,
          text: 'Européen',
          IconUrl: './assets/picto/cuisine/picto_europeen.png',
          BgCol: '#68A89E',
          idsSousQuestions: []
        },
        {
          id: 68,
          text: 'Américain',
          IconUrl: './assets/picto/cuisine/Picto_amerique_N_S.png',
          BgCol: '#68A89E',
          idsSousQuestions: []
        },
        {
          id: 69,
          text: 'Africain',
          IconUrl: './assets/picto/cuisine/picto_africain.png',
          BgCol: '#68A89E',
          idsSousQuestions: []
        },
//-----------fin cuisine
]};