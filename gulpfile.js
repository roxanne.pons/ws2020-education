'use strict';

// Déclaration des dépendances
// var $ = require('gulp-load-plugins')({lazy:ture});
var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();

// Sass utilise le compiler de node.js
sass.compiler = require('node-sass');

// Création de ma tâche pour compiler mes fichier sass en css
gulp.task('sass', function () {
    return gulp.src('./src/sass/**/*.scss') // les * permettent de faire des dossiers dans src et ils seront pris en compte
        .pipe(sourcemaps.init()) //donne le nom du fichier css en inspectant
        .pipe(autoprefixer()) //les webkit prefix
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./build/css'))
        .pipe(browserSync.stream()); //pour y acceder en dehors d'une tache
});
//on deplace le fichier html dans le build aussi pour avoir le style
gulp.task('html', function (){
    return gulp.src('./src/**/*.html')
        .pipe(gulp.dest('./build'))
});

//ca watch pour des modifs dans les fichiers et reload si modif
gulp.task('watch', function () {
    browserSync.init({
        server: {
            baseDir: "./build"
        }
    });

    gulp.watch('./src/sass/**/*.scss', gulp.series('sass'))
    gulp.watch('./src/*.html', gulp.series('html')).on('change', browserSync.reload);
});

//one task to do them 
gulp.task('default', gulp.parallel('sass', 'html', 'watch'));